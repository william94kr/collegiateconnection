#!/usr/bin/python3

""" For scraping data -- universities, companies, and cities. """

import os
import csv
import wikipedia
import json
from bs4 import BeautifulSoup
from dotenv import load_dotenv
from requests import get
from pprint import pprint

us_state_to_abbrev = {
    "Alabama": "AL",
    "Alaska": "AK",
    "Arizona": "AZ",
    "Arkansas": "AR",
    "California": "CA",
    "Colorado": "CO",
    "Connecticut": "CT",
    "Delaware": "DE",
    "Florida": "FL",
    "Georgia": "GA",
    "Hawaii": "HI",
    "Idaho": "ID",
    "Illinois": "IL",
    "Indiana": "IN",
    "Iowa": "IA",
    "Kansas": "KS",
    "Kentucky": "KY",
    "Louisiana": "LA",
    "Maine": "ME",
    "Maryland": "MD",
    "Massachusetts": "MA",
    "Michigan": "MI",
    "Minnesota": "MN",
    "Mississippi": "MS",
    "Missouri": "MO",
    "Montana": "MT",
    "Nebraska": "NE",
    "Nevada": "NV",
    "New Hampshire": "NH",
    "New Jersey": "NJ",
    "New Mexico": "NM",
    "New York": "NY",
    "North Carolina": "NC",
    "North Dakota": "ND",
    "Ohio": "OH",
    "Oklahoma": "OK",
    "Oregon": "OR",
    "Pennsylvania": "PA",
    "Rhode Island": "RI",
    "South Carolina": "SC",
    "South Dakota": "SD",
    "Tennessee": "TN",
    "Texas": "TX",
    "Utah": "UT",
    "Vermont": "VT",
    "Virginia": "VA",
    "Washington": "WA",
    "West Virginia": "WV",
    "Wisconsin": "WI",
    "Wyoming": "WY",
    "District of Columbia": "DC",
    "American Samoa": "AS",
    "Guam": "GU",
    "Northern Mariana Islands": "MP",
    "Puerto Rico": "PR",
    "United States Minor Outlying Islands": "UM",
    "U.S. Virgin Islands": "VI",
}

# invert the dictionary
abbrev_to_us_state = dict(map(reversed, us_state_to_abbrev.items()))


# Setting up environment + getting environment variables.

load_dotenv()

CLEARBIT_KEY = os.environ["CLEARBIT_API_KEY"]
COLLEGE_KEY = os.environ["COLLEGE_API_KEY"]


def get_wiki_image(search_term: str):
    """Gets the main image for the wiki page. None otherwise."""
    try:
        result = wikipedia.search(search_term, results=1)
        wikipedia.set_lang("en")
        wkpage = wikipedia.WikipediaPage(title=result[0])
        soup = BeautifulSoup(wkpage.html())
        tds = soup.findAll("td", {"class": "infobox-image"})
        for td in tds:
            image_element = td.findChildren("img", recursive=True)[0]
            if image_element:
                return f'https:{image_element["src"]}'
        return None
    except:
        return None


def scrape_colleges(per_page: int, pages: int, filename: str):
    """Scrapes colleges and writes them into the specified csv file."""
    # Trying not to overload csv file.
    assert per_page * pages <= 2000

    latest_fields = [
        "student.size",
        "cost.tuition.in_state",
        "cost.tuition.out_of_state",
        "student.demographics.women",
        "student.demographics.men",
        "admissions.admission_rate.overall",
        "admissions.sat_scores.average.overall",
        "admissions.act_scores.midpoint.cumulative",
        "student.demographics.race_ethnicity.white",
        "student.demographics.race_ethnicity.black",
        "student.demographics.race_ethnicity.hispanic",
        "student.demographics.race_ethnicity.asian",
        "student.demographics.race_ethnicity.aian",
        "student.demographics.race_ethnicity.nhpi",
        "student.demographics.race_ethnicity.unknown",
        "student.demographics.race_ethnicity.two_or_more",
        "student.share_firstgeneration",
        "student.demographics.avg_family_income",
        "student.retention_rate.four_year.full_time_pooled",
        "earnings.10_yrs_after_entry.median",
    ]

    fields = [
        "school.name",
        "school.city",
        "school.school_url",
        "school.state",
        "school.zip",
        "id",
        "location.lat",
        "location.lon",
    ]

    changed_latest_fields = list(map(lambda x: f"latest.{x}", latest_fields))

    all_fields = fields + changed_latest_fields

    base_url = "https://api.data.gov/ed/collegescorecard/v1/schools.json"

    # To ensure, we're getting relevant data -- filter only by schools that offer bachelors + graduate degrees.
    # sorted in ascending order of acceptance rate. More selective schools are near the top.
    filters = "school.degrees_awarded.predominant=3,4&sort=latest.admissions.admission_rate.overall"

    data = []
    for page in range(pages):
        query_params = f'?api_key={COLLEGE_KEY}&page={page}&per_page={per_page}&{filters}&fields={",".join(all_fields)}'
        page_resp = get(base_url + query_params).json()

        for college in page_resp["results"]:
            row = []
            for val in college.values():
                if val in abbrev_to_us_state:
                    val = abbrev_to_us_state[val]
                row.append(val)
            data.append(row)

    for college in data:
        name = college[-8]
        print(name)
        college.append(get_wiki_image(name))
        print(college[-1])

    # Opening/writing to csv file now.
    with open(filename, "w") as colleges_file:
        writer = csv.writer(colleges_file)
        writer.writerow(latest_fields + fields + ["image"])
        writer.writerows(data)

    print("Write succeeded.")


if __name__ == "__main__":
    scrape_colleges(150, 10, "colleges8.csv")
