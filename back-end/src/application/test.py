import re
from unittest import main, TestCase
import requests
import json

from requests.api import request # Python's library used for sending requests and getting back responses

BASE = "https://api.collegiateconnection.me"


class Tests(TestCase):

    ### cities

    # testing the numbers of data with no query param (from cities (default-> page: 1 per_page: 20))
    def test_cities(self):
        req = requests.get(BASE + "/cities") # send request for the unit test
        assert req.status_code == 200

        result = req.json()
        assert result["page"] == 1
        assert result["per_page"] == 20
        assert result["total"] is not None

    # testing the numbers of data (from cities with page: 2, per_page: 10)
    def test_cities_num(self):
        req = requests.get(BASE + "/cities?page=2&per_page=10")
        assert req.status_code == 200

        result = req.json()
        assert result["page"] == 2
        assert result["per_page"] == 10
        assert result["total"] is not None

    # testing the numbers of data and the data over the last page (from cities with page: 12, per_page: 20)
    def test_cities_no_data(self):
        req = requests.get(BASE + "/cities?page=12&per_page=20")
        assert req.status_code == 200

        result = req.json()
        assert result["page"] == 12
        assert result["per_page"] == 0
        assert result["cities"] == []

    # testing the data with valid id (from cities with id: 4945283)
    def test_cities_id(self):
        req = requests.get(BASE + "/cities/5391811")
        assert req.status_code == 200

        result = req.json()
        assert result["id"] == "5391811"

    # testing filtering of Cities by state
    def test_cities_filtering_state(self):
        req = requests.get(BASE + "/cities?state=Texas")
        assert req.status_code == 200

        result = req.json()
        for city in result["cities"]:  # city is a python dict
            assert city["state"] == "Texas"

    # testing filtering of Cities by timezone
    def test_cities_filtering_timezone(self):
        req = requests.get(BASE + "/cities?timezone=PST")
        assert req.status_code == 200

        result = req.json()
        for city in result["cities"]:
            assert city["timezone"] == "PST"

    # testing sorting for cities
    def test_cities_sorting(self):
        req = requests.get(BASE + "/cities?sort=economy")
        assert req.status_code == 200
        result = req.json()
        cities_list = result["cities"]

        for i in range(1, len(cities_list)):
            prev = cities_list[i - 1]
            cur = cities_list[i]
            assert prev["economy"] <= cur["economy"]

    # testing searching for a city
    def test_cities_search(self):
        req = requests.get(BASE + "/cities?search=Texas")
        assert req.status_code == 200
        result = req.json()
        searched = result["cities"]

        for i in searched:
            assert i["state"] == "Texas"

    ### colleges ###

    # testing the numbers of data with no query param (from colleges (default-> page: 1 per_page: 20))
    def test_colleges(self):
        req = requests.get(BASE + "/colleges")
        assert req.status_code == 200

        result = req.json()
        assert result["page"] == 1
        assert result["per_page"] == 20
        assert result["total"] is not None

    # testing the numbers of data (from colleges with page: 2, per_page: 10)
    def test_colleges_num(self):
        req = requests.get(BASE + "/colleges?page=2&per_page=10")
        assert req.status_code == 200

        result = req.json()
        assert result["page"] == 2
        assert result["per_page"] == 10
        assert result["total"] is not None

    # testing the numbers of data and the data over the last page (from colleges with page: 30, per_page: 20)
    def test_colleges_no_data(self):
        req = requests.get(BASE + "/colleges?page=30&per_page=20")
        assert req.status_code == 200

        result = req.json()
        assert result["page"] == 30
        assert result["per_page"] == 0
        assert result["colleges"] == []

    # testing the data with valid id (from colleges with id: 121257)
    def test_colleges_id(self):
        req = requests.get(BASE + "/colleges/121257")
        assert req.status_code == 200

        result = req.json()
        assert result["id"] == "121257"

    # testing filtering of Colleges by sat range
    def test_colleges_filtering_sat_range(self):
        req = requests.get(BASE + "/colleges?sat=1300-1600")
        assert req.status_code == 200

        result = req.json()
        for college in result["colleges"]:
            sat = college["admissions_sat_scores_average_overall"]
            assert sat >= 1300 and sat <= 1600

    # testing filtering of Colleges by act range
    def test_colleges_filtering_act_range(self):
        req = requests.get(BASE + "/colleges?act=28-32")
        assert req.status_code == 200

        result = req.json()
        for college in result["colleges"]:
            act = college["admissions_act_scores_midpoint_cumulative"]
            assert act >= 28 and act <= 32

    # testing filtering of Colleges by acceptance rate
    def test_colleges_filtering_acceptancerate(self):
        req = requests.get(BASE + "/colleges?accept_rate=4-10")
        assert req.status_code == 200

        result = req.json()
        epsilon = 0.0000001
        for college in result["colleges"]:
            acceptance_rate = college["admissions_admission_rate_overall"]
            assert acceptance_rate >= (4 / 100 - epsilon) and acceptance_rate <= (
                10 / 100 + epsilon
            )

    # testing sorting for colleges
    def test_colleges_sorting(self):
        req = requests.get(BASE + "/colleges?sort=-sat")
        assert req.status_code == 200
        result = req.json()
        colleges_list = result["colleges"]
        epsilon = 0.0000001

        for i in range(1, len(colleges_list)):
            prev = colleges_list[i - 1]
            cur = colleges_list[i]
            assert (prev["admissions_sat_scores_average_overall"] + epsilon) >= (
                cur["admissions_sat_scores_average_overall"] - epsilon
            )

    # testing searching for a college
    def test_colleges_search(self):
        req = requests.get(BASE + "/colleges?search=university of texas at austin")
        assert req.status_code == 200
        result = req.json()
        terms = "university of texas at austin".split(" ")

        for college in result["colleges"]:
            lowercase_name = college["name"].lower()
            assert any(
                [t in lowercase_name for t in terms]
            )  # check if any search parameter is in the name of the college

    ### companies ###

    # testing the numbers of data with no query param (from companies (default-> page: 1 per_page: 20))
    def test_companies(self):
        req = requests.get(BASE + "/companies")
        assert req.status_code == 200

        result = req.json()
        assert result["page"] == 1
        assert result["per_page"] == 20
        assert result["total"] is not None

    # testing the numbers of data (from companies with page: 2, per_page: 10)
    def test_companies_num(self):
        req = requests.get(BASE + "/companies?page=2&per_page=10")
        assert req.status_code == 200

        result = req.json()
        assert result["page"] == 2
        assert result["per_page"] == 10
        assert result["total"] is not None

    # testing the numbers of data and the data over the last page (from companies with page: 29, per_page: 20)
    def test_companies_no_data(self):
        req = requests.get(BASE + "/companies?page=29&per_page=20")
        assert req.status_code == 200

        result = req.json()
        assert result["page"] == 29
        assert result["per_page"] == 0
        assert result["companies"] == []

    # testing the data with valid id (from companies with id: 373)
    def test_companies_id(self):
        req = requests.get(BASE + "/companies/373")
        assert req.status_code == 200

        result = req.json()
        assert result["company_id"] == 373

    # testing filtering of Companies by industry
    def test_companies_filtering_industry(self):
        req = requests.get(BASE + "/companies?industry=Real Estate")
        assert req.status_code == 200
        result = req.json()

        for company in result["companies"]:
            assert company["industry"] == "Real Estate"

    # testing filtering of Companies by industry
    def test_companies_filtering_revenue(self):
        req = requests.get(BASE + "/companies?annual_revenue=$1B-$10B")
        assert req.status_code == 200
        result = req.json()
        options = {"$1B-$10B"}

        for company in result["companies"]:
            assert company["estimated_annual_revenue"] in options

    # testing sorting for companies
    def test_companies_sorting(self):
        req = requests.get(BASE + "/companies?sort=-alexa_rank")
        assert req.status_code == 200
        result = req.json()
        companies_list = result["companies"]

        for i in range(1, len(companies_list)):
            prev = companies_list[i - 1]
            cur = companies_list[i]
            assert int(prev["alexa_rank"]) >= int(cur["alexa_rank"])

    # testing searching for a company
    def test_company_search(self):
        req = requests.get(BASE + "/companies?search=Amazon")
        assert req.status_code == 200
        result = req.json()
        searched = result["companies"]

        for i in searched:
            assert i["name"] == "Amazon"


if __name__ == "__main__":
    main()
