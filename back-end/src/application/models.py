from flask import Flask, request, make_response, jsonify
from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from marshmallow import fields, post_dump
from db_setup import initialize_db

app = Flask(__name__)

app.config["JSON_SORT_KEYS"] = False
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False

CORS(app)
db = initialize_db(app)
ma = Marshmallow(app)

link_companies_colleges = db.Table(
    "link_companies_colleges",
    db.Column(
        "company_id",
        db.Integer,
        db.ForeignKey("companies.company_id"),
        primary_key=True,
    ),
    db.Column("college_id", db.String, db.ForeignKey("colleges.id"), primary_key=True),
)


class Cities(db.Model):
    """db Model for cities"""

    __tablename__ = "cities"

    id = db.Column(db.Integer, primary_key=True)

    # Searchables
    name = db.Column(db.String, nullable=False)
    state = db.Column(db.String, nullable=False)
    full_name = db.Column(db.String, nullable=False)

    population = db.Column(db.Integer, nullable=False)
    timezone = db.Column(db.String, nullable=True)
    latitude = db.Column(db.Float, nullable=False)
    longitude = db.Column(db.Float, nullable=False)

    # Indices.
    housing = db.Column(db.Float, nullable=True)
    cost_of_living = db.Column(db.Float, nullable=True)
    startups = db.Column(db.Float, nullable=True)
    commute = db.Column(db.Float, nullable=True)
    business_freedom = db.Column(db.Float, nullable=True)
    safety = db.Column(db.Float, nullable=True)
    healthcare = db.Column(db.Float, nullable=True)
    education = db.Column(db.Float, nullable=True)
    environmental_quality = db.Column(db.Float, nullable=True)
    economy = db.Column(db.Float, nullable=True)
    taxation = db.Column(db.Float, nullable=True)
    internet_access = db.Column(db.Float, nullable=True)
    leisure_culture = db.Column(db.Float, nullable=True)
    tolerance = db.Column(db.Float, nullable=True)
    outdoors = db.Column(db.Float, nullable=True)
    travel_connectivity = db.Column(db.Float, nullable=True)

    web_image = db.Column(db.String, nullable=True)

    mobile_image = db.Column(db.String, nullable=True)

    # JSON fields used for model-instance connections with other model instances
    companies_ids = db.Column(db.JSON, nullable=False)
    colleges_ids = db.Column(db.JSON, nullable=False)


class Colleges(db.Model):
    """db Model for colleges"""

    __tablename__ = "colleges"

    # Searchables
    id = db.Column(db.String, primary_key=True)
    city_id = db.Column(
        db.Integer, db.ForeignKey("cities.id"), nullable=False
    )  # one to many relationship (college(s) --> one city)
    name = db.Column(db.String, nullable=False)
    student_size = db.Column(db.Integer, nullable=True)
    school_school_url = db.Column(db.String, nullable=True)
    school_city = db.Column(db.String, nullable=False)
    school_zip = db.Column(db.String, nullable=True)
    location_lat = db.Column(db.Float, nullable=False)
    location_lon = db.Column(db.Float, nullable=False)

    # Filterables.
    student_demographics_women = db.Column(db.Float, nullable=True)
    student_demographics_men = db.Column(db.Float, nullable=True)
    admissions_admission_rate_overall = db.Column(db.Float, nullable=True)
    admissions_sat_scores_average_overall = db.Column(db.Float, nullable=True)
    admissions_act_scores_midpoint_cumulative = db.Column(db.Float, nullable=True)
    student_demographics_race_ethnicity_white = db.Column(db.Float, nullable=True)
    student_demographics_race_ethnicity_black = db.Column(db.Float, nullable=True)
    student_demographics_race_ethnicity_hispanic = db.Column(db.Float, nullable=True)
    student_demographics_race_ethnicity_asian = db.Column(db.Float, nullable=True)
    student_demographics_race_ethnicity_aian = db.Column(db.Float, nullable=True)
    student_demographics_race_ethnicity_nhpi = db.Column(db.Float, nullable=True)
    student_demographics_race_ethnicity_unknown = db.Column(db.Float, nullable=True)
    student_demographics_race_ethnicity_two_or_more = db.Column(db.Float, nullable=True)
    student_share_firstgeneration = db.Column(db.Float, nullable=True)
    earnings_10_yrs_after_entry_median = db.Column(db.Integer, nullable=True)
    student_demographics_avg_family_income = db.Column(db.Integer, nullable=True)
    student_retention_rate_four_year_full_time_pooled = db.Column(
        db.Float, nullable=True
    )
    cost_tuition_in_state = db.Column(db.Integer, nullable=True)
    cost_tuition_out_of_state = db.Column(db.Integer, nullable=True)
    state_name = db.Column(db.String, nullable=False)

    image = db.Column(db.String, nullable=True)
    # JSON fields used for model-instance connections with other model instances
    companies_ids = db.Column(db.JSON, nullable=False)
    companies = db.relationship(
        "Companies", secondary=link_companies_colleges, backref="colleges"
    )


class Companies(db.Model):
    """db Model for companies"""

    __tablename__ = "companies"

    company_id = db.Column(db.Integer, primary_key=True)
    city_id = db.Column(
        db.Integer, db.ForeignKey("cities.id"), nullable=False
    )  # one to many relationship (compan(y)(ies) --> one city)

    # Company name/ Founded year/ Headquarters location or address/
    # Logo/ Phone number/ Email address/ Twitter handle
    # Searchables
    name = db.Column(db.String, nullable=False)
    founded_year = db.Column(db.Integer, nullable=True)
    location_street_name = db.Column(db.String, nullable=True)
    location_street_number = db.Column(db.String, nullable=True)
    location_sub_premise = db.Column(db.String, nullable=True)
    location_city = db.Column(db.String, nullable=False)
    location_postal_code = db.Column(db.String, nullable=True)
    locaton_state = db.Column(db.String, nullable=False)
    location_state_code = db.Column(db.String, nullable=False)
    location_country = db.Column(db.String, nullable=False)
    location_country_code = db.Column(db.String, nullable=False)
    location_latitude = db.Column(db.Float, nullable=False)
    location_longitude = db.Column(db.Float, nullable=False)
    logo = db.Column(db.String, nullable=True)
    phone_number = db.Column(db.String, nullable=True)
    email = db.Column(db.String, nullable=True)
    twitter_handle = db.Column(db.String, nullable=True)
    alexa_rank = db.Column(db.Integer, nullable=True)

    # Filterable
    category_tags = db.Column(db.String, nullable=False)
    industry = db.Column(db.String, nullable=False)
    employees = db.Column(db.Integer, nullable=True)
    estimated_annual_revenue = db.Column(db.String, nullable=True)
    twitter_id = db.Column(db.Float, nullable=True)
    linkedin_handle = db.Column(db.String, nullable=True)
    description = db.Column(db.String, nullable=True)

    # back ref
    """ city_id = db.Column(db.Integer, nullable=False) """
    # JSON fields used for model-instance connections with other model instances
    colleges_ids = db.Column(db.JSON, nullable=False)
