import os
from flask import Flask, request, make_response, jsonify
from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from marshmallow import fields, post_dump
from dotenv import load_dotenv


def initialize_db(app: Flask) -> SQLAlchemy:
    """Initializes the DB source."""
    load_dotenv()

    # get confidential information from our .env file
    DB_USER = os.getenv("DB_USER")
    DB_PASSWORD = os.getenv("DB_PASSWORD")
    DB_HOST = os.getenv("DB_HOST")
    DB_PORT = os.getenv("DB_PORT")
    DB_NAME = os.getenv("DB_NAME")

    URI = f"postgresql+psycopg2://{DB_USER}:{DB_PASSWORD}@{DB_HOST}:{DB_PORT}/{DB_NAME}"

    app.config["SQLALCHEMY_DATABASE_URI"] = URI

    return SQLAlchemy(app) # make the SQL Alchemy application instance


def drop_db(db):
    """Use with caution, it drops the entire database."""

    db.session.remove() # removes the session, which would have been used for adding model instances to our db
    db.drop_all()


def create_db(db):
    """Creates the Database schema."""

    db.create_all() # create all of the models (i.e. SQL Alchemy classes) that we have
