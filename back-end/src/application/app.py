from flask import Flask, request, make_response, jsonify, render_template
from jinja2 import TemplateNotFound
from flask import jsonify, render_template
from schemas import (
    city_schema,
    city_schemas,
    college_schema,
    college_schemas,
    company_schema,
    company_schemas,
)
from models import db, app, Cities, Colleges, Companies
from typing import Tuple
from sqlalchemy import nullslast, desc, or_, func, cast, String
from sqlalchemy.orm import aliased

# ---------------- HELPER FUNCTIONS --------------------

not_found = lambda message: make_response(jsonify(message), 404)
extract_default = lambda dict, key, default: dict[key][0] if key in dict else default


def get_query_bounds(query_string: str) -> Tuple[int, int]:
    '''
    Extracts the upper and lower bounds from the query string.
    Returns a tuple with the upper and lower bounds
    '''
    if query_string is None:
        return query_string

    lower, upper = map(int, query_string.split("-"))

    return (lower, upper)


def filter_ignore(query, column, range_val):
    '''
    Unpacks an exclusive upper-bound and exclusive lower-bound and then enforces
    that all items in the query are within the range [upper-bound, lower-bound]
    '''
    if range_val is None:
        return query

    lower, higher = range_val
    return query.filter(column >= lower).filter(column <= higher)


def filter_ignore_single(query, column, val):
    '''
    filter by rows that have the column equal to val
    '''
    if val is None:
        return query
    return query.filter(column == val)


def custom_sort(query, sortable_attributes, sort):
    '''
    Sorting using a custom sort by using SQL Alchemy's order_by
    we are sorting numbers effectively
    '''
    if sort is None:
        return query

    for attribute in sortable_attributes:
        if attribute in sort:
            key = sortable_attributes[attribute]
            if sort[0] == "-":
                query = query.order_by(nullslast(desc(key)))
            else:
                query = query.order_by(nullslast(key))
            break
    return query


# ---------------- ROUTE DEFINITIONS --------------------


@app.route("/", defaults={"path": ""})
@app.route("/<path:path>")
def invalid_endpoint(path):
    """Missing resource endpoint."""

    return not_found("Not a valid endpoint.")


@app.route("/cities", methods=["GET"])
def cities():
    """
    Retrieving all cities (view all).
    Needs `page` and `per_page` parameters, defaulted to 1 and 20 if not
    provided.
    """
    queries = request.args.to_dict(flat=False)

    page_num = int(extract_default(queries, "page", 1))
    per_page = int(extract_default(queries, "per_page", 20))
    state = extract_default(queries, "state", None)
    timezone = extract_default(queries, "timezone", None)
    sort = extract_default(queries, "sort", None)
    search = extract_default(queries, "search", None)

    # Filterable
    query = db.session.query(Cities)
    query = filter_ignore_single(query, Cities.state, state)
    query = filter_ignore_single(query, Cities.timezone, timezone)

    # Searching.
    if search is not None:
        keywords = list(map(lambda x: x.lower(), search.split()))

        matches = []
        for keyword in keywords:
            matches.append(func.lower(Cities.name).contains(keyword))
            matches.append(func.lower(Cities.state).contains(keyword))
            matches.append(func.lower(Cities.full_name).contains(keyword))
            matches.append(func.lower(Cities.timezone).contains(keyword))
            matches.append(cast(Cities.latitude, String()).contains(keyword))
            matches.append(cast(Cities.longitude, String()).contains(keyword))

        query = query.filter(or_(*matches))

    # Sorting.
    sortable_attributes = {
        "economy": Cities.economy,
        "housing": Cities.housing,
        "cost_of_living": Cities.cost_of_living,
        "latitude": Cities.latitude,
        "longitude": Cities.longitude,
    }

    query = custom_sort(query, sortable_attributes, sort)

    all_cities = None
    total = 0
    try:
        all_cities = (
            query
            # db.session.query(Cities)
            .order_by(Cities.population.desc()).paginate(
                page=page_num, per_page=per_page
            )
        )
        total = query.count()
    except Exception as e:
        app.logger.info(e)

    if not all_cities or not total:
        return jsonify({"cities": [], "page": page_num, "per_page": 0, "total": 0})
    result = city_schemas.dump(all_cities.items)
    return jsonify(
        {
            "cities": result,
            "page": page_num,
            "per_page": len(all_cities.items),
            "total": total,
        }
    )


@app.route("/cities/<int:id>", methods=["GET"])
def cities_id(id: int):
    """
    Retrieves city by ID. `id` must be provided.
    """
    city = Cities.query.get(id)

    if not city:
        return not_found({"message": f"id ({id}) not found for cities."}) # error message if no city with the passed in id
    return city_schema.jsonify(city)


@app.route("/colleges", methods=["GET"])
def colleges():
    """
    Retrieving all colleges (view all).
    Needs `page` and `per_page` parameters, defaulted to 1 and 20 if not
    provided.
    """
    queries = request.args.to_dict(flat=False)

    # Extracting query parameters.
    page_num = int(extract_default(queries, "page", 1))
    per_page = int(extract_default(queries, "per_page", 20))
    sort = extract_default(queries, "sort", None)
    search = extract_default(queries, "search", None)

    # Filterable
    sat_range = get_query_bounds(extract_default(queries, "sat", None))
    act_range = get_query_bounds(extract_default(queries, "act", None))
    acceptance_rate = get_query_bounds(extract_default(queries, "accept_rate", None))
    in_state_tuition = get_query_bounds(
        extract_default(queries, "in_state_tuition", None)
    )
    out_state_tuition = get_query_bounds(
        extract_default(queries, "out_state_tuition", None)
    )
    public = extract_default(queries, "public", None)
    state = extract_default(queries, "state", None)

    if acceptance_rate is not None:
        acceptance_rate = list(map(lambda x: x / 100, acceptance_rate))

    query = db.session.query(Colleges)
    if state:
        query = query.filter(Colleges.state_name == state)

    if public:
        if public == "true":
            query = query.filter(
                Colleges.cost_tuition_in_state != Colleges.cost_tuition_out_of_state
            )
        elif public == "false":
            query = query.filter(
                Colleges.cost_tuition_in_state == Colleges.cost_tuition_out_of_state
            )

    query = filter_ignore(
        query, Colleges.admissions_sat_scores_average_overall, sat_range
    )
    query = filter_ignore(
        query, Colleges.admissions_act_scores_midpoint_cumulative, act_range
    )
    query = filter_ignore(
        query, Colleges.admissions_admission_rate_overall, acceptance_rate
    )
    query = filter_ignore(query, Colleges.cost_tuition_in_state, in_state_tuition)
    query = filter_ignore(query, Colleges.cost_tuition_out_of_state, out_state_tuition)

    # Searching.
    if search is not None:
        keywords = list(map(lambda x: x.lower(), search.split()))

        matches = []
        for keyword in keywords:
            matches.append(func.lower(Colleges.name).contains(keyword))
            matches.append(func.lower(Colleges.school_city).contains(keyword))
            matches.append(func.lower(Colleges.school_school_url).contains(keyword))
            matches.append(func.lower(Colleges.school_zip).contains(keyword))
            matches.append(func.lower(Colleges.state_name).contains(keyword))

        query = query.filter(or_(*matches))

    # Sorting.
    sortable_attributes = {
        "sat": Colleges.admissions_sat_scores_average_overall,
        "act": Colleges.admissions_act_scores_midpoint_cumulative,
        "accept_rate": Colleges.admissions_admission_rate_overall,
        "in_state_tuition": Colleges.cost_tuition_in_state,
        "out_state_tuition": Colleges.cost_tuition_out_of_state,
    }

    query = custom_sort(query, sortable_attributes, sort)

    # Executing query.

    all_colleges = None
    total = 0
    try:
        all_colleges = query.paginate(page=page_num, per_page=per_page)
        total = query.count()
    except Exception as e:
        app.logger.error(e)

    if not all_colleges:
        return jsonify({"colleges": [], "page": page_num, "per_page": 0, "total": 0})
    result = college_schemas.dump(all_colleges.items)
    return jsonify(
        {
            "colleges": result,
            "page": page_num,
            "per_page": len(all_colleges.items),
            "total": total,
        }
    )


@app.route("/colleges/<int:id>", methods=["GET"])
def colleges_id(id: int):
    """
    Retrieves college by ID. `id` must be provided.
    """
    college = Colleges.query.get(str(id))

    if not college:
        return not_found({"message": f"id ({id}) not found for colleges."}) # return an error message if the college is not found
    return college_schema.jsonify(college)


@app.route("/companies", methods=["GET"])
def companies():
    """
    Retrieving all companies (view all).
    Needs `page` and `per_page` parameters, defaulted to 1 and 20 if not
    provided.
    """
    queries = request.args.to_dict(flat=False)

    # Extracting arguments.

    page_num = int(extract_default(queries, "page", 1))
    per_page = int(extract_default(queries, "per_page", 20))
    annual_revenue = extract_default(queries, "annual_revenue", None)
    industry = extract_default(queries, "industry", None)
    sort = extract_default(queries, "sort", "alexa")
    search = extract_default(queries, "search", None)

    query = db.session.query(Companies)

    # Filtering.
    query = filter_ignore_single(query, Companies.industry, industry)
    query = filter_ignore_single(
        query, Companies.estimated_annual_revenue, annual_revenue
    )

    # Searching.
    if search is not None:
        keywords = list(map(lambda x: x.lower(), search.split()))

        matches = []
        for keyword in keywords:
            matches.append(func.lower(Companies.name).contains(keyword))
            matches.append(func.lower(Companies.location_city).contains(keyword))
            matches.append(func.lower(Companies.locaton_state).contains(keyword))
            matches.append(func.lower(Companies.location_state_code).contains(keyword))
            matches.append(func.lower(Companies.location_street_name).contains(keyword))
            matches.append(func.lower(Companies.location_postal_code).contains(keyword))

        query = query.filter(or_(*matches))

    # Sorting.
    sortable_attributes = {
        "name": Companies.name,
        "alexa": Companies.alexa_rank,
        "employees": Companies.employees,
    }

    query = custom_sort(query, sortable_attributes, sort)

    all_companies = None
    total = 0

    try:
        all_companies = query.paginate(page=page_num, per_page=per_page)
        total = query.count()
    except Exception as e:
        app.logger.error(e)

    if not all_companies:
        return jsonify({"companies": [], "page": page_num, "per_page": 0, "total": 0})

    result = company_schemas.dump(all_companies.items)
    # return results from doing pagination
    return jsonify(
        {
            "companies": result,
            "page": page_num,
            "per_page": len(all_companies.items),
            "total": total,
        }
    )


@app.route("/companies/<int:id>", methods=["GET"])
def companies_id(id: int):
    """
    Retrieves a company by ID. `id` must be provided.
    """
    company = Companies.query.get(id)

    if not company:
        return not_found({"message": f"id ({id}) not found for colleges."}) # return an error message if the company is not found
    return company_schema.jsonify(company)


if __name__ == "__main__":
    # Run on localhost 5000 for local.
    app.run(host="127.0.0.1", port=8080, debug=False)
