import models
from flask import Flask
from marshmallow import fields
from flask_sqlalchemy import SQLAlchemy


class CollegeSchema(models.ma.Schema):

    id = fields.String(required=True)
    name = fields.String(required=True)
    student_size = fields.Integer(required=False)
    school_school_url = fields.String(required=False)
    school_city = fields.String(required=True)
    school_zip = fields.String(required=False)
    location_lat = fields.Float(required=True)
    location_lon = fields.Float(required=True)

    # Filterables.
    student_demographics_women = fields.Float(required=False)
    student_demographics_men = fields.Float(required=False)
    admissions_admission_rate_overall = fields.Float(required=False)
    admissions_sat_scores_average_overall = fields.Float(required=False)
    admissions_act_scores_midpoint_cumulative = fields.Float(required=False)
    student_demographics_race_ethnicity_white = fields.Float(required=False)
    student_demographics_race_ethnicity_black = fields.Float(required=False)
    student_demographics_race_ethnicity_hispanic = fields.Float(required=False)
    student_demographics_race_ethnicity_asian = fields.Float(required=False)
    student_demographics_race_ethnicity_aian = fields.Float(required=False)
    student_demographics_race_ethnicity_nhpi = fields.Float(required=False)
    student_demographics_race_ethnicity_unknown = fields.Float(required=False)
    student_demographics_race_ethnicity_two_or_more = fields.Float(required=False)
    student_share_firstgeneration = fields.Float(required=False)
    school_carnegie_undergrad = fields.Integer(required=False)
    earnings_10_yrs_after_entry_median = fields.Integer(required=False)
    student_demographics_avg_family_income = fields.Integer(required=False)
    student_retention_rate_four_year_full_time_pooled = fields.Float(required=False)
    cost_tuition_in_state = fields.Integer(required=False)
    cost_tuition_out_of_state = fields.Integer(required=False)
    image = fields.String(required=False)

    city_id = fields.String(required=True)
    state_name = fields.String(required=True)

    companies_ids = fields.String(required=True)


class CitySchema(models.ma.Schema):
    id = fields.Str(required=True)
    name = fields.Str(required=True)
    state = fields.Str(required=True)

    full_name = fields.Str(required=False)
    population = fields.Int(required=False)
    latitude = fields.Float(required=False)
    timezone = fields.Str(required=False)
    longitude = fields.Float(required=False)
    housing = fields.Float(required=False)
    cost_of_living = fields.Float(required=False)
    startups = fields.Float(required=False)
    commute = fields.Float(required=False)
    business_freedom = fields.Float(required=False)
    safety = fields.Float(required=False)
    healthcare = fields.Float(required=False)
    education = fields.Float(required=False)
    environmental_quality = fields.Float(required=False)
    economy = fields.Float(required=False)
    economy = fields.Float(required=False)
    taxation = fields.Float(required=False)
    internet_access = fields.Float(required=False)
    leisure_culture = fields.Float(required=False)
    tolerance = fields.Float(required=False)
    outdoors = fields.Float(required=False)
    travel_connectivity = fields.Float(required=False)
    image = fields.Int(required=True)
    web_image = fields.Str(required=False)
    mobile_image = fields.Str(required=False)

    companies_ids = fields.Str(required=True)
    colleges_ids = fields.Str(required=True)


class BusinessSchema(models.ma.Schema):
    company_id = fields.Int(required=True)
    name = fields.Str(required=True)
    founded_year = fields.Int(required=False)
    location_street_name = fields.String(required=False)
    location_street_number = fields.String(required=False)

    location_sub_premise = fields.String(required=False)
    location_city = fields.String(required=True)
    location_postal_code = fields.String(required=False)
    location_state = fields.String(required=True)

    location_state_code = fields.String(required=True)
    location_latitude = fields.Float(required=True)
    location_longitude = fields.Float(required=True)
    logo = fields.String(required=False)
    phone_number = fields.String(required=False)
    email = fields.String(required=False)
    twitter_handle = fields.String(required=False)
    alexa_rank = fields.String(required=False)

    # Filterable
    category_tags = fields.String(required=True)
    industry = fields.String(required=True)
    employees = fields.Integer(required=False)
    estimated_annual_revenue = fields.String(required=False)
    twitter_id = fields.String(required=False)
    linkedin_handle = fields.String(required=False)
    description = fields.String(required=False)

    city_id = fields.String(required=True)

    colleges_ids = fields.String(required=True)


# Initialize schemas
city_schema = CitySchema()
city_schemas = CitySchema(many=True)
college_schema = CollegeSchema()
college_schemas = CollegeSchema(many=True)
company_schema = BusinessSchema()
company_schemas = BusinessSchema(many=True)
