import unittest
import os

from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.chrome.options import Options
from selenium.webdriver import Remote
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait


class Tests(unittest.TestCase):
    def setUp(self) -> None:
        chrome_options = Options()
        chrome_options.add_argument("--no-sandbox")
        chrome_options.add_argument("--headless")

        self.driver = webdriver.Chrome(
            ChromeDriverManager().install(), options=chrome_options
        )

        self.driver.maximize_window()

    def tearDown(self):
        self.driver.quit()

    def test_home(self):
        self.driver.get("https://www.collegiateconnection.me/")
        self.driver.implicitly_wait(2)

        # taking us home
        assert "https://www.collegiateconnection.me/" in self.driver.current_url

    def test_buttons_exist(self):

        self.driver.get("https://www.collegiateconnection.me/")
        self.driver.implicitly_wait(2)

        # taking us home
        assert "https://www.collegiateconnection.me/" in self.driver.current_url

        button_one = "#root > div > div.justify-content-center.align-items-center > div.centered-card > div > div > div.row.justify-content-center > div:nth-child(1) > a > button > span"
        button_two = "#root > div > div.justify-content-center.align-items-center > div.centered-card > div > div > div.row.justify-content-center > div:nth-child(2) > a > button > span"
        button_three = "#root > div > div.justify-content-center.align-items-center > div.centered-card > div > div > div.row.justify-content-center > div:nth-child(3) > a > button > span"

        # navbar first element
        elem = self.driver.find_element_by_css_selector(button_one)
        assert elem.text == "Explore Universities"
        elem = self.driver.find_element_by_css_selector(button_two)
        assert elem.text == "Explore Companies"
        elem = self.driver.find_element_by_css_selector(button_three)
        assert elem.text == "Explore Cities"

    def test_navbar(self):
        # Testing click functionality of navbar.

        self.driver.get("https://www.collegiateconnection.me/")
        self.driver.implicitly_wait(2)

        about_us = "#root > div > div:nth-child(1) > div > ul > li:nth-child(3) > span.ant-menu-title-content > a"
        universities = "#root > div > div:nth-child(1) > div > ul > li:nth-child(4) > span.ant-menu-title-content > a"

        WebDriverWait(self.driver, 20).until(
            EC.element_to_be_clickable((By.CSS_SELECTOR, about_us))
        ).click()
        assert "https://www.collegiateconnection.me/about" in self.driver.current_url

        WebDriverWait(self.driver, 20).until(
            EC.element_to_be_clickable((By.CSS_SELECTOR, universities))
        ).click()
        assert "https://www.collegiateconnection.me/colleges" in self.driver.current_url

    def test_about_us(self):

        # Testing correct information on about us.

        self.driver.get("https://www.collegiateconnection.me/")
        about_us = "#root > div > div:nth-child(1) > div > ul > li:nth-child(3) > span.ant-menu-title-content > a"
        title_divider_one = "#root > div > div:nth-child(3) > div.ant-divider.ant-divider-horizontal.ant-divider-with-text.ant-divider-with-text-center > span > article > h2"
        title_divider_two = "#root > div > div:nth-child(4) > span > article > h2"

        WebDriverWait(self.driver, 20).until(
            EC.element_to_be_clickable((By.CSS_SELECTOR, about_us))
        ).click()
        self.driver.implicitly_wait(1)

        team_divider_one = self.driver.find_element_by_css_selector(title_divider_one)
        team_divider_two = self.driver.find_element_by_css_selector(title_divider_two)

        assert "The Team" in team_divider_one.text
        assert "APIs/Tools Used" in team_divider_two.text

    def test_available_colleges(self):

        # Test Universities page

        self.driver.get("https://www.collegiateconnection.me/")

        universities_button = "#root > div > div.justify-content-center.align-items-center > div.centered-card > div > div > div.row.justify-content-center > div:nth-child(1) > a > button > span"

        WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable((By.CSS_SELECTOR, universities_button))
        ).click()
        assert "https://www.collegiateconnection.me/colleges" in self.driver.current_url

        title = "#root > div > div.page-header-wrapper > div > div.ant-page-header-heading > div > span.ant-page-header-heading-title"
        assert (
            "Explore Colleges" == self.driver.find_element_by_css_selector(title).text
        )

    def test_available_cities(self):

        # Test Cities page

        self.driver.get("https://www.collegiateconnection.me/")

        cities_button = "#root > div > div.justify-content-center.align-items-center > div.centered-card > div > div > div.row.justify-content-center > div:nth-child(3) > a > button > span"

        WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable((By.CSS_SELECTOR, cities_button))
        ).click()

        assert (
            "https://www.collegiateconnection.me/locations" in self.driver.current_url
        )

        title = "#root > div > div.page-header-wrapper > div > div.ant-page-header-heading > div > span.ant-page-header-heading-title"
        assert "Explore Cities" == self.driver.find_element_by_css_selector(title).text

    # def test_team_members_present(self):

    #     # Test clicking on card.

    #     self.driver.get("https://www.collegiateconnection.me/")

    #     about_us = '#root > div > div:nth-child(1) > div > ul > li:nth-child(3) > span.ant-menu-title-content > a'
    #     WebDriverWait(self.driver, 20).until(EC.element_to_be_clickable((By.CSS_SELECTOR, about_us))).click()

    #     container = '#root > div > div:nth-child(3) > div.grid-container'
    #     WebDriverWait(self.driver, 20).until(EC.presence_of_element_located((By.CSS_SELECTOR, container)))

    #     assert len(self.driver.find_element_by_css_selector(container)) == 5

    def test_individual_city(self):

        # Test clicking on card.

        self.driver.get("https://www.collegiateconnection.me/locations")

        card = "#root > div > div.grid-container > div:nth-child(2) > div"

        # Click should succeed.

        WebDriverWait(self.driver, 20).until(
            EC.element_to_be_clickable((By.CSS_SELECTOR, card))
        ).click()

    def test_city_info_present(self):

        # Test clicking on card.

        self.driver.get("https://www.collegiateconnection.me/")

        city_button = "#root > div > div.justify-content-center.align-items-center > div.centered-card > div > div > div.row.justify-content-center > div:nth-child(3) > a > button > span"

        WebDriverWait(self.driver, 20).until(
            EC.element_to_be_clickable((By.CSS_SELECTOR, city_button))
        ).click()

        entry = "#root > div > div.grid-container > div:nth-child(1) > div > div.ant-card-body > div > div > div.ant-card-meta-title"
        WebDriverWait(self.driver, 20).until(
            EC.presence_of_element_located((By.CSS_SELECTOR, entry))
        )

        assert "Los Angeles" in self.driver.find_element_by_css_selector(entry).text

    def test_pagination_exists(self):

        self.driver.get("https://www.collegiateconnection.me/companies")

        total = "#root > div > div:nth-child(3) > div > div > div > ul > li.ant-pagination-total-text"

        WebDriverWait(self.driver, 20).until(
            EC.presence_of_element_located((By.CSS_SELECTOR, total))
        )

        assert "Companies" in self.driver.find_element_by_css_selector(total).text

    def test_search_exists(self):

        search = "#root > div > div.page-header-wrapper > div > div.ant-page-header-content > span > span > span.ant-input-group-addon > button > span"

        city_button = "#root > div > div.justify-content-center.align-items-center > div.centered-card > div > div > div.row.justify-content-center > div:nth-child(3) > a > button > span"
        self.driver.get("https://www.collegiateconnection.me/")
        WebDriverWait(self.driver, 20).until(
            EC.element_to_be_clickable((By.CSS_SELECTOR, city_button))
        ).click()

        WebDriverWait(self.driver, 20).until(
            EC.presence_of_element_located((By.CSS_SELECTOR, search))
        )

        assert "Search" in self.driver.find_element_by_css_selector(search).text
    
    def test_search_university(self):
        self.driver.get("https://www.collegiateconnection.me/colleges/?page=1&perPage=20&search=rice")

        rice_uni_city = '#root > div > div.grid-container > div:nth-child(2) > div > div.ant-card-body > div > div > div.ant-card-meta-description > span:nth-child(3) > span'
        WebDriverWait(self.driver, 20).until(
            EC.presence_of_element_located((By.CSS_SELECTOR, rice_uni_city))
        )
        assert "Houston" in self.driver.find_element_by_css_selector(rice_uni_city).text
    
    def test_search_company(self):
        self.driver.get("https://www.collegiateconnection.me/companies/?search=microsoft")

        microsoft = '#root > div > div:nth-child(3) > div > div > div > div > div > div > table > tbody > tr.ant-table-row.ant-table-row-level-0 > td:nth-child(2) > span > mark'
        WebDriverWait(self.driver, 20).until(
            EC.presence_of_element_located((By.CSS_SELECTOR, microsoft))
        )
        assert "Microsoft" in self.driver.find_element_by_css_selector(microsoft).text
    
    def test_search_city(self):
        self.driver.get("https://www.collegiateconnection.me/locations/?search=dallas")

        dallas = '#root > div > div.grid-container > div > div > div.ant-card-body > div > div > div.ant-card-meta-title > span > span'
        WebDriverWait(self.driver, 20).until(
            EC.presence_of_element_located((By.CSS_SELECTOR, dallas))
        )
        assert "Dallas" in self.driver.find_element_by_css_selector(dallas).text

    def test_filter(self):
        self.driver.get('https://www.collegiateconnection.me/colleges/?page=1&perPage=20&public=false')

        filter_key = '#root > div > div.page-header-wrapper > div > div.ant-page-header-content > div:nth-child(5) > div:nth-child(2) > div:nth-child(2) > div > div > span.ant-select-selection-placeholder'
        WebDriverWait(self.driver, 20).until(
            EC.presence_of_element_located((By.CSS_SELECTOR, filter_key))
        )

        assert "Private" in self.driver.find_element_by_css_selector(filter_key).text
    
    def test_sort(self):
        self.driver.get('https://www.collegiateconnection.me/companies/?page=1&perPage=20&sort=-alexa_rank')

        first_entry = '#root > div > div:nth-child(3) > div > div > div > div > div > div > table > tbody > tr:nth-child(2) > td:nth-child(3)'
        second_entry = '#root > div > div:nth-child(3) > div > div > div > div > div > div > table > tbody > tr:nth-child(3) > td:nth-child(3)'
        third_entry = '#root > div > div:nth-child(3) > div > div > div > div > div > div > table > tbody > tr:nth-child(4) > td:nth-child(3)'

        WebDriverWait(self.driver, 20).until(
            EC.presence_of_element_located((By.CSS_SELECTOR, first_entry))
        )

        val_1 = int(self.driver.find_element_by_css_selector(first_entry).text)
        val_2 = int(self.driver.find_element_by_css_selector(second_entry).text)
        val_3 = int(self.driver.find_element_by_css_selector(third_entry).text)

        assert val_1 > val_2
        assert val_2 > val_3

    

if __name__ == "__main__":
    unittest.main()
