import React from "react";
import "antd/dist/antd.css";
import "./styles/card.css";
import { Card } from "antd";
import { useHistory } from "react-router-dom";

const { Meta } = Card;

function MoreCard(params) {
  const { data } = params;
  const history = useHistory();

  // Function to take user to related search query page.
  const handleClick = () => {
    let route = `/${data.category}`;

    if (data.searchStr !== undefined && data.searchStr !== null) {
      route += `?search=${data.searchStr}`;
    }

    // Push to history.
    history.push(route);
  };

  // Returning component.
  return (
    <>
      <Card className="itemcard-no-shadow" onClick={handleClick}>
        <Meta
          title={<b className="bolded">See All</b>}
          description={
            <div>
              <b> Click to see all {data.nums} matches </b>
            </div>
          }
        />
      </Card>
    </>
  );
}
export default MoreCard;
