import { Pie } from "@ant-design/charts";

// Returning stats about student ethnic composition.
const generateStatsEthnicity = (data) => {
  const ret = [
    {
      ethnicity: "Black",
      value: data.student_demographics_race_ethnicity_black || 0,
    },
    {
      ethnicity: "White",
      value: data.student_demographics_race_ethnicity_white || 0,
    },
    {
      ethnicity: "Hispanic",
      value: data.student_demographics_race_ethnicity_hispanic || 0,
    },
    {
      ethnicity: "Asian",
      value: data.student_demographics_race_ethnicity_asian || 0,
    },
    {
      ethnicity: "Native Hawaiian and Pacific Islander",
      value: data.student_demographics_race_ethnicity_nhpi || 0,
    },
    {
      ethnicity: "American Indian and Alaskan Native",
      value: data.student_demographics_race_ethnicity_aian || 0,
    },
    {
      ethnicity: "Two or More",
      value: data.student_demographics_race_ethnicity_two_or_more || 0,
    },
  ];

  let total = 0;
  for (let stat in ret) {
    total += stat.value;
  }

  // Make up differnece with unknown ethnicity.
  if (total < 1) {
    ret.push({
      ethnicity: "Unknown",
      value: 1 - total,
    });
  }

  return ret;
};

const generateStatsGender = (data) => {
  const ret = [
    {
      gender: "Male",
      value: data.student_demographics_men ? data.student_demographics_men : 0,
    },
    {
      gender: "Female",
      value: data.student_demographics_women
        ? data.student_demographics_women
        : 0,
    },
  ];

  let total = 0;
  for (let stat in ret) {
    total += stat.value;
  }

  if (total < 1) {
    ret.push({
      gender: "Unknown",
      value: 1 - total,
    });
  }

  return ret;
};

const DemoPieEthnicity = (rawData) => {
  const data = generateStatsEthnicity(rawData);

  const config = {
    data,
    meta: {
      ethnicity: {
        alias: "Ethnicity",
        range: [0, 1],
      },
      value: {
        alias: "a",
        formatter: (v) => {
          return `${parseFloat((v * 100).toPrecision(1))}%`;
        },
      },
    },
    angleField: "value",
    colorField: "ethnicity",

    interactions: [{ type: "element-active" }],
    state: {
      active: {
        animate: { duration: 100, easing: "easeLinear" },
        style: {
          lineWidth: 2,
          stroke: "#000",
        },
      },
    },
    style: {
      fillOpacity: 0.1,
      inactiveOpacity: 0.4,
    },
    label: {
      autoHide: true,
    },
    legend: {
      layout: "horizontal",
      position: "top",
    },
    padding: "auto",
  };
  return <Pie {...config} style={{ padding: "20px" }} />;
};

const DemoPieGender = (rawData) => {
  const data = generateStatsGender(rawData);

  const config = {
    data,
    meta: {
      ethnicity: {
        alias: "Gender",
        range: [0, 1],
      },
      value: {
        alias: "a",
        formatter: (v) => {
          return `${parseFloat((v * 100).toPrecision(2))}%`;
        },
      },
    },
    angleField: "value",
    colorField: "gender",

    interactions: [{ type: "element-active" }],
    state: {
      active: {
        animate: { duration: 100, easing: "easeLinear" },
        style: {
          lineWidth: 2,
          stroke: "#000",
        },
      },
    },
    style: {
      fillOpacity: 0.1,
      inactiveOpacity: 0.4,
    },
    label: {
      autoHide: true,
    },
    legend: {
      layout: "horizontal",
      position: "top",
    },
    padding: "auto",
  };
  return <Pie {...config} style={{ padding: "20px" }} />;
};

export { DemoPieEthnicity, DemoPieGender };
