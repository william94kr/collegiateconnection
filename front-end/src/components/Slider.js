import React, { useState } from 'react';

import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-slider/dist/css/bootstrap-slider.css';

import RangeSlider from 'react-bootstrap-range-slider';


// FIXME: Need to pass down fileArr object from TrendReports component as prop. Some of the variables down below should be retrieved from that.
const Slide = (props) => {


    var minNum = props.min
    var maxNum = props.max

    // FIXME: value is a static year value. This should be generated dynamically, this code would fail with any other dataset.
    const [value, setValue] = useState(2000);
    const [finalValue, setFinalValue] = useState(null);


    return (
        <div>
            <RangeSlider
                value={value}
                onChange={changeEvent => setValue(changeEvent.target.value)}
                onAfterChange={e => setFinalValue(e.target.value)}
                min={minNum}
                max={maxNum}
                step={1}
            />


        </div>

    );
};
export default Slide;
