import React from "react";
import "antd/dist/antd.css";
import "./styles/card.css";
import { Card } from "antd";
import { useHistory } from "react-router-dom";
import Highlighter from "react-highlight-words";

const { Meta } = Card;

const CompanyCard = (params) => {
  const { data } = params;
  const history = useHistory();

  // Taking user to instance page for the company.
  const handleClick = () => {
    history.push(`/companies/${data.company_id}`);
  };
  return (
    <>
      <Card
        cover={
          <img
            alt="Company"
            src={
              data.logo ||
              "https://cdn1.iconfinder.com/data/icons/location-basic-1-semi-gray/468/Layer11-512.png"
            }
            className="icon-image"
          />
        }
        className={data.className ? data.className : "itemcard"}
        onClick={handleClick}
      >
        <Meta
          title={
            <Highlighter
              searchWords={data.searchString || []}
              textToHighlight={data.name}
            />
          }
          description={
            <>
              <b> City: </b>{" "}
              {
                <Highlighter
                  searchWords={data.searchString || []}
                  textToHighlight={data.location_city}
                />
              }{" "}
              <br />
              <b> State: </b>{" "}
              {
                <Highlighter
                  searchWords={data.searchString || []}
                  textToHighlight={data.location_state_code}
                />
              }{" "}
              <br />
              <b> Industry: </b> <br />{" "}
              {
                <Highlighter
                  searchWords={data.searchString || []}
                  textToHighlight={data.industry}
                />
              }{" "}
              <br />
              <b> Employees: </b> {data.employees || "N/A"} <br />
              <b> Alexa Rank </b> {data.alexa_rank || "N/A"} <br />
              <b> Annual Estimated Revenue </b>{" "}
              {data.estimated_annual_revenue || "N/A"} <br />
              <br />
            </>
          }
        />
      </Card>
    </>
  );
};
export default CompanyCard;
