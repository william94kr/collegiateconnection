import { React } from "react";
import "antd/dist/antd.css";
import { Select } from "antd";
const { Option } = Select;

const Filter = (funcParameters) => {
  let { options, placeholder, params, setParams, query, setPage } =
    funcParameters;
  return (
    <>
      <div style={{ paddingTop: "10px", paddingRight: "10px" }}>
        <Select
          showSearch
          style={{
            maxHeight: "30vh",
            overflow: "auto",
            width: "200px",
          }}
          placeholder={placeholder}
          onSelect={(val) => {
            if (val === null) {
              let another = {};
              for (let prop in params) {
                if (prop === "page") {
                  another[prop] = 1;
                } else if (prop === query) {
                  another[prop] = undefined;
                } else {
                  another[prop] = params[prop];
                }
              }
              setParams(another);
              setPage(1);
            } else {
              setParams({ ...params, [query]: val, page: 1 });
              setPage(1);
            }
          }}
        >
          {options.map((option) => {
            return <Option value={option[1]}> {option[0]} </Option>;
          })}
        </Select>
      </div>
    </>
  );
};

export default Filter;
