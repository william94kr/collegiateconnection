import React from "react";
import "antd/dist/antd.css";
import "./styles/card.css";
import { Card } from "antd";

const { Meta } = Card;
class ToolCard extends React.Component {
  render() {
    return (
      <>
        <a href={`${this.props.link}`}>
          <Card
            cover={
              <img
                alt="Tool"
                src={this.props.image}
                width="85%"
                margin="15px"
              />
            }
            className="itemcard"
          >
            <Meta
              title={this.props.name}
              description={
                <div>
                  <b>Description: </b> {this.props.description}
                </div>
              }
            />
          </Card>
        </a>
      </>
    );
  }
}
export default ToolCard;
