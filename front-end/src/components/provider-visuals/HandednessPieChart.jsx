import React, { useState, useEffect } from "react";
import { Spin } from "antd";
import { useHistory } from "react-router-dom";
import {
    PieChart, 
    Pie,
    Tooltip,
    Cell,
    ResponsiveContainer 
} from "recharts";
import axios from "axios";

// import { view_all_companies } from '../library/client';

const view_players_baseball = async() => {
    const query = 'https://api.citieslovebaseball.me/player?pageSize=500'
    const data = (
        await axios.get(query)
    ).data.players

    return data
}

function HandednessPieChart() {

  const [loading, setLoading] = useState(true)
  const [pieData, setPieData] = useState([])
  const history = useHistory();
  const COLORS = ['#0088FE', '#00C49F', '#FFBB28', '#FF8042'];

  useEffect(() => {

    const data = async () => {
        try {
            setLoading(true)
            const players = await view_players_baseball()
            setLoading(false)
                        

            const hands = players
                .map(player => player.batting_side) // get all media types
                .filter((batting_side, index, array) => array.indexOf(batting_side) === index); // filter out duplicates
            const ret = hands
                .map(batting_side => ({
                    name: batting_side,
                    value: players.filter(player => player.batting_side === batting_side).length
                }));
        

            console.log(ret)
            setPieData(ret || [])
        } catch (e) {
            history.push('/void')
        }
    }

    data()


  }, [])

  return (
    loading ? (
        <div style={{ textAlign: 'center' }}> <Spin size='large' tip='loading visualization...'/> </div>
    ) :  <div style={{
        display: 'flex'
    }}>
        <ResponsiveContainer width='99%' height={500}>
        <PieChart width={400} height={400}>
          <Pie
            dataKey="value"
            isAnimationActive={false}
            data={pieData}
            cx="50%"
            cy="50%"
            outerRadius={150}
            fill="#8884d8"
            label
            
          >
          {pieData.map((entry, index) => (
            <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />
          ))}
          </Pie>
          {/* <Pie dataKey="value" data={pieData} cx={500} cy={200} innerRadius={40} outerRadius={80} fill="#82ca9d" /> */}
          <Tooltip />
        </PieChart>
        </ResponsiveContainer>
    </div>

  );
}

export default HandednessPieChart;
