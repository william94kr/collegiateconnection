import React, { useState, useEffect } from "react";
import { Spin } from "antd";
import { useHistory } from "react-router-dom";
import { BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, ResponsiveContainer } from "recharts";
import axios from "axios";

const view_teams_baseball = async() => {
    const query = 'https://api.citieslovebaseball.me/team?pageSize=30'
    const data = (
        await axios.get(query)
    ).data.teams

    return data
}

function HomerunBar() {

  const [loading, setLoading] = useState(true)
  const [plotData, setPlotData] = useState([])
  const history = useHistory();

  useEffect(() => {

    const data = async () => {
        try {
            setLoading(true)
            const fetched = await view_teams_baseball()
            setLoading(false)

            const ret = fetched.map( team => {
                let obj = {}
                obj.name = team.name
                obj['Home Runs'] = team.home_runs

                return obj
            })

            console.log(ret)
            setPlotData(ret || [])
        } catch (e) {
            console.error(e)
            history.push('/void')
        }
    }

    data()

  }, [])

  return (
    loading ? (
        <div style={{ textAlign: 'center' }}> <Spin size='large' tip='loading visualization...'/> </div>
    ) : <div style={{
        display: 'flex'
    }}>
        <ResponsiveContainer width='99%' height={500}>
                <BarChart
                width={800}
                height={600}
                data={plotData}
                layout="horizontal"
                margin={{ bottom: 10 }}
                >
                <CartesianGrid strokeDasharray="3 3" />
                <XAxis
                type="category"
                dataKey="name"
                tick={false}
                label={{
                    value: 'Teams'
                }}
                />
                <YAxis
                type="number"
                label={{
                    value: "Homeruns",
                    angle: -90,
                    position: "insideLeft"
                }}
                />
                <Tooltip />
                <Bar dataKey="Home Runs" fill="#1E88E5" />
            </BarChart>
        </ResponsiveContainer>

    </div>


  );
}

export default HomerunBar;
