import { React, useState, useEffect } from "react";
import "antd/dist/antd.css";
import "./styles/location.css";
import {
  Pagination,
  PageHeader,
  Input,
  Select,
  Typography,
  Spin,
  Row,
  Col,
  Divider,
  Empty,
} from "antd";
import { Descriptions } from "antd";
import LocationCard from "../components/LocationCard";
import { view_all_cities } from "../library/client";
import { useHistory } from "react-router";
import { Menu } from "antd"; // ant design components are used
import { DownOutlined } from "@ant-design/icons";
import stateNames from "./static/States.json";
import Filter from "../components/Filter";
import {
  StringParam,
  NumberParam,
  useQueryParams,
  withDefault,
} from "use-query-params";
const { Search } = Input;

const value_mapping_sort = {
  economy: "Economy (Asc.)",
  "-economy": "Economy (Desc.)",
  housing: "Housing (Asc.)",
  "-housing": " Housing (Desc.)",
  cost_of_living: "Cost of Living (Asc.)",
  "-cost_of_living": "Cost of Living (Desc.)",
  latitude: "Latitude (Asc.)",
  "-latitude": "Latitude (Desc.)",
  longitude: "Longitude (Asc.)",
  "-longitude": "Longitude (Desc.)",
};

/* Pagination code inspired by: https://gitlab.com/forbesye/fitsbits/-/blob/master/front-end/src/views/Politicians/GridView.js. */
export default function Location() {
  const [loading, setLoading] = useState(true);
  const [total, setTotal] = useState(0);
  const [page, setPage] = useState(1);
  const [perPage, setPerPage] = useState(20);
  const [citiesData, setCitiesData] = useState([]);
  const [params, setParams] = useQueryParams({
    page: withDefault(NumberParam, 1),
    perPage: withDefault(NumberParam, 20),
    search: StringParam,
    sort: StringParam,
    public: StringParam,
    state: StringParam,
    timezone: StringParam,
  });
  const { Option } = Select;
  const { Title } = Typography;
  const history = useHistory();
  const [highlight, setHighlight] = useState([]);

  const cityState = (
    <Menu>
      {stateNames.map((data) => (
        <Menu.Item key="1"> {data.name} </Menu.Item>
      ))}
    </Menu>
  );

  useEffect(() => {
    const data = async () => {
      try {
        setLoading(true);
        const fetched = await view_all_cities(page, perPage, params);
        setLoading(false);
        setCitiesData(fetched.cities || []);
        setTotal(fetched.total || 0);
        setPage(page);
      } catch (e) {
        history.push("/void");
      }
    };

    data();
  }, [page, perPage, params, highlight]);
  return (
    <>
      <div className="page-header-wrapper">
        <PageHeader
          ghost={false}
          title={<b className="bolded"> Explore Cities </b>}
          subTitle="Browse/search for available cities"
        >
          <Descriptions size="small" column={3}>
            <Descriptions>
              Learn more about the city of your dreams!
            </Descriptions>
          </Descriptions>
          <Search
            placeholder={params.search ? params.search : "Search"}
            allowClear
            enterButton="Search"
            size="large"
            onSearch={(value) => {
              setParams({ ...params, search: value, page: 1 });
              setHighlight(value.split(" "));
            }}
          />
          <Divider />
          <div>
            <Title level={5}>
              {" "}
              <i> Search Customization </i>{" "}
            </Title>
          </div>
          <Row>
            <Col>
              <div style={{ paddingTop: "10px" }}>
                <b> Sorting </b>
              </div>
              <div style={{ paddingTop: "10px", paddingRight: "10px" }}>
                <Select
                  showSearch
                  style={{ width: 200 }}
                  placeholder={
                    params.sort ? value_mapping_sort[params.sort] : "Sort by"
                  }
                  onSelect={(val) => {
                    if (val === null) {
                      let another = {};
                      for (let prop in params) {
                        if (prop === "page") {
                          another[prop] = 1;
                        } else if (prop === "sort") {
                          another[prop] = undefined;
                        } else {
                          another[prop] = params[prop];
                        }
                      }
                      setParams(another);
                      return;
                    }
                    setParams({ ...params, sort: val, page: 1 });
                  }}
                >
                  <Option value={null}> Default </Option>
                  <Option value="economy">Economy (Asc.) </Option>
                  <Option value="-economy">Economy (Desc.) </Option>
                  <Option value="housing">Housing (Asc.) </Option>
                  <Option value="-housing">Housing (Desc.) </Option>
                  <Option value="cost_of_living">Cost of Living (Asc.) </Option>
                  <Option value="-cost_of_living">
                    Cost of Living (Desc.){" "}
                  </Option>
                  <Option value="latitude"> Latitude (Asc.) </Option>
                  <Option value="-latitude"> Latitude (Desc.) </Option>
                  <Option value="longitude"> Longitude (Asc.) </Option>
                  <Option value="-longitude"> Longitude (Desc.) </Option>
                </Select>
              </div>
            </Col>
            <Col>
              <div style={{ paddingTop: "10px" }}>
                <b> State </b>
              </div>
              <Filter
                options={stateNames
                  .map((data) => [data.name, data.name])
                  .concat([["All", null]])}
                placeholder={params.state ? params.state : "States"}
                params={params}
                setParams={setParams}
                query={"state"}
                setPage={setPage}
              />
            </Col>
            <Col>
              <div style={{ paddingTop: "10px" }}>
                <b> Timezones </b>
              </div>
              {/* filter based on timezone */}
              <Filter
                options={[
                  ["EDT", "EDT"],
                  ["CDT", "CDT"],
                  ["MDT", "MDT"],
                  ["PDT", "PDT"],
                  ["MST", "MST"],
                  ["HST", "HST"],
                  ["All", null],
                ]}
                placeholder={params.timezone ? params.timezone : "Timezone"}
                params={params}
                setParams={setParams}
                query={"timezone"}
                setPage={setPage}
              />
            </Col>
          </Row>
        </PageHeader>
      </div>
      {loading ? (
        <div style={{ textAlign: "center" }}>
          {" "}
          <Spin size="large" tip="loading cities..." />{" "}
        </div>
      ) : (
        <>
          {citiesData.length > 0 ? (
            <div className="grid-container">
              {citiesData.map((city) => (
                <div className="grid-item">
                  {" "}
                  {/* display the location card*/}
                  <LocationCard
                    data={{ ...city, searchString: highlight }}
                  />{" "}
                </div>
              ))}
            </div>
          ) : (
            <div style={{ textAlign: "center" }}>
              {" "}
              <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} />{" "}
            </div>
          )}
          <div className="paddingWrapper">
            <Pagination
              showSizeChanger
              total={total}
              pageSize={params.perPage}
              current={params.page}
              onChange={(num, size) =>
                setParams({ ...params, page: num, perPage: size })
              }
              style={{
                padding: "30px",
                display: "flex",
                justifyContent: "flex-end",
              }}
              showTotal={(total) => `${total} Cities`}
            />
          </div>
        </>
      )}
    </>
  );
}
