import React from "react";
import "antd/dist/antd.css";
import "./styles/home.css";
import { Typography, Carousel, Card } from "antd";
import { Button } from "antd";
import "../components/styles/card.css";

import { Link } from "react-router-dom";

const { Title } = Typography;
const { Meta } = Card;

class Home extends React.Component {
  // display the cards needed for our Home page, based on ant design cards
  render() {
    return (
      <>
        <div className=" justify-content-center align-items-center">
          <Carousel classname="carousel" dotPosition="top" autoplay>
            <div>
              <div className="cover-image-1" />
            </div>
            <div>
              <div className="cover-image-2" />
            </div>
            <div>
              <div className="cover-image-3" />
            </div>
          </Carousel>
          <div className="centered-card">
            <Card className="introcard">
              <Typography>
                <Title>
                  {" "}
                  Welcome to <b className="bolded title">Collegiate</b>{" "}
                  <div className="title"> Connection! </div>{" "}
                </Title>
              </Typography>
              {/* display card with our 'mission statement */}
              <Meta
                description={`
                        Collegiate Connection helps you make informed decisions about your future with nearby schools and their locations by 
                        compiling information from local Universities and business databases. As students of the 
                        University of Texas - Austin, life after college decisions are a big deal to us! Information about
                        their universities should accessible to all students. Creating this website is our first step 
                        in helping everyone make informed decisions for life after college.
                    `}
              ></Meta>

              <br />
              <div className="row justify-content-center">
                <div className="p-2 col-xs-12 col-sm-6 col-md-6 col-lg-4 justify-content-center">
                  <Link to="/colleges">
                    <Button type="primary"> Explore Universities </Button>
                  </Link>
                </div>
                <div className="p-2 col-xs-12 col-sm-6 col-md-6 col-lg-4 justify-content-center">
                  <Link to="/companies">
                    <Button type="primary"> Explore Companies </Button>
                  </Link>
                </div>
                <div className="p-2 col-xs-12 col-sm-6 col-md-6 col-lg-4 justify-content-center">
                  <Link to="/locations">
                    <Button type="primary"> Explore Cities </Button>
                  </Link>
                </div>
                <div className="p-2 col-xs-12 col-sm-6 col-md-6 col-lg-4 justify-content-center">
                  <a href="https://youtu.be/NBPFT25iOjA">
                    <Button type="primary"> Presentation </Button>
                  </a>
                </div>
              </div>
            </Card>
          </div>
        </div>
      </>
    );
  }
}
export default Home;
