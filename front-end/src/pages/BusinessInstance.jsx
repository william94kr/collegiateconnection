import { React, useState, useEffect } from "react";
import "antd/dist/antd.css";
import "./styles/location.css";
import { PageHeader, Popover, Spin, Card, Divider, Typography } from "antd";
import {
  PhoneOutlined,
  LinkedinOutlined,
  MailOutlined,
} from "@ant-design/icons";
import "./styles/storeInstance.css";
import "./styles/storeList.css";
import CollegeCard from "../components/CollegeCard";
import { view_college, view_company } from "../library/client";
import { useLocation } from "react-router-dom";
import "../components/styles/card.css";

import Meta from "antd/lib/card/Meta";
import "bootstrap/dist/css/bootstrap.min.css";
import MapContainer from "../components/MapContainer";
import { Link, useHistory } from "react-router-dom";

const { Title } = Typography;

const giveActions = (data) => {
  let ret = [];
  // options for different social media / contact points

  if (data.linkedin_handle === undefined || data.linkedin_handle === null) {
    ret.push(
      <Popover content="LinkedIn Info not available">
        <LinkedinOutlined key="linkedin" />
      </Popover>
    );
  } else {
    ret.push(
      <a href={`https://www.linkedin.com/${data.linkedin_handle}`}>
        <LinkedinOutlined key="linkedin" />
      </a>
    );
  }

  ret.push(
    <Popover content={data.email || "Email Info not available"}>
      <MailOutlined key="email" />
    </Popover>
  );

  ret.push(
    <Popover content={data.phone_number || "Phone Info not available"}>
      <PhoneOutlined key="email" />
    </Popover>
  );
  return ret;
};

/* Pagination code inspired by: https://gitlab.com/forbesye/fitsbits/-/blob/master/front-end/src/views/Politicians/GridView.js. */
export default function BusinessInstance() {
  const { state } = useLocation();

  const [loading, setLoading] = useState(true);
  const [data, setData] = useState({});

  const [loadingColleges, setLoadingColleges] = useState(true);
  const [collegesIds, setCollegesIds] = useState([]);

  const history = useHistory();
  
  // useEffect hook used for the process of pagination
  useEffect(() => {
    const fetcher = async () => {
      try {
        var id = undefined;
        if (state === undefined) {
          id = window.location.pathname.split("/")[2];
        } else {
          id = state.identifier;
        }
        const info = await view_company(id);

        setData(info);
        setLoading(false);

        let ids = [];
        const ids_obj = JSON.parse(info.colleges_ids);
        for (let [_, value] of Object.entries(ids_obj)) {
          ids.push(value);
          if (ids.length === 20) {
            break;
          }
        }

        const all_objs = await Promise.all(ids.map((id) => view_college(id)));
        setCollegesIds(all_objs);
        setLoadingColleges(false);
      } catch (e) {
        history.push("/void");
      }
    };
    fetcher();
  }, []);

  return (
    <>
      <div className="store-list-page-header">
        <PageHeader
          ghost={false}
          onBack={() => window.history.back()}
          title="Company Profile"
          subTitle="See if this company is right match"
        />
      </div>
      {loading ? (
        <div style={{ textAlign: "center" }}>
          <Spin size="large" tip={`Loading Information for Company... `} />
        </div>
      ) : (
        <>
          <Divider>
            {" "}
            <Title level={3}> General Information </Title>{" "}
          </Divider>
          <div className="col-md-6 mx-auto col-lg-6 col-sm-12 col-xs-12">
            <Card
              cover={
                <img
                  alt="Company logo"
                  src={
                    data.logo ||
                    "https://www.wodonnell.com/wp-content/uploads/2019/02/business-placeholder.png"
                  }
                  className="business-image"
                  style={{
                    padding: "15px",
                  }}
                />
              }
              style={{
                padding: "20px",
                margin: "auto",
              }}
              className="store-info-card"
              actions={giveActions(data)}
            >
              <Meta
                title={<Title level={4}> {data.name} </Title>}
                description={
                  <>
                    <b> Industry: </b> {data.industry || "N/A"} <br />
                    <b> Founded Year: </b> {data.founded_year || "N/A"} <br />
                    <b> Estimated Revenue: </b>{" "}
                    {data.estimated_annual_revenue || "N/A"} <br />
                    <b> Number of Employees: </b> {data.employees || "N/A"}{" "}
                    <br />
                    <Popover
                      title="What's this?"
                      content="Alexa ranking is a measure of popularity for websites."
                    >
                      <div style={{ cursor: "pointer", display: "inline" }}>
                        <b> Alexa Rank: </b> {data.alexa_rank || "N/A"}
                      </div>
                    </Popover>{" "}
                    <br />
                    <b> City: </b>{" "}
                    <Link to={`/locations/${data.city_id}`}>
                      {" "}
                      {data.location_city}{" "}
                    </Link>
                    <br />
                    <b> State: </b> {data.location_state_code} <br />
                    <b> Postal Code: </b> {data.location_postal_code || "N/A"}{" "}
                    <br />
                    <br />
                    <b> Categories: </b> <br />
                    {data.category_tags.replaceAll("'", "").slice(1, -1)} <br />
                    <br />
                    {data.description || "No summary is available."}
                  </>
                }
                style={{
                  textAlign: "center",
                }}
              />
            </Card>
          </div>
          <Divider>
            {" "}
            <Title level={3}> Map </Title>{" "}
          </Divider>
          <MapContainer
            {...{
              latitude: data.location_latitude,
              longitude: data.location_longitude,
            }}
          />
          <Divider>
            {" "}
            <Title level={3}> Nearby Colleges </Title>{" "}
          </Divider>
          {loadingColleges ? (
            <div style={{ textAlign: "center" }}>
              <Spin
                size="large"
                tip={`Loading Information for Nearby Colleges... `}
              />
            </div>
          ) : (
            <>
              <div className="grid-container">
                {collegesIds.map((college) => (
                  <div className="grid-item">
                    {" "}
                    <CollegeCard data={{ ...college }} />{" "}
                  </div>
                ))}
              </div>
            </>
          )}
        </>
      )}
    </>
  );
}
