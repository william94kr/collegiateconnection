import { React, useState, useEffect } from "react";
import "antd/dist/antd.css";
import "./styles/location.css";
import {
  PageHeader,
  Spin,
  Card,
  Divider,
  Typography,
  Progress,
  Avatar,
  Table,
  Button,
} from "antd";
import { view_college, view_company } from "../library/client";
import { useLocation } from "react-router-dom";
import "./styles/storeInstance.css";
import "../components/styles/card.css";
import "./styles/storeList.css";
import {
  DemoPieEthnicity,
  DemoPieGender,
} from "../components/DemographicsPieChart";
import Meta from "antd/lib/card/Meta";
import "bootstrap/dist/css/bootstrap.min.css";
import MapContainer from "../components/MapContainer";
import { Link, useHistory } from "react-router-dom";
import { SelectOutlined } from "@ant-design/icons";

const { Title } = Typography;

// JSX for the college instance pages
const CollegeInfo = (data) => {
  return (
    <>
      <div>
        <div className="container-fluid">
          <div className="row g-5 justify-content-md-center">
            <div className="col-xs-12 col-sm-12 col-md-5 col-lg-4">
              <div className="p-3 text-center">
                <Card
                  cover={
                    <img
                      alt="College"
                      src={
                        data.image ||
                        "https://cdn1.iconfinder.com/data/icons/location-basic-1-semi-gray/468/Layer11-512.png"
                      }
                      className="college-image"
                      style={{
                        padding: "15px",
                      }}
                    />
                  }
                  className="store-info-card"
                  actions={[
                    <a href={`https://${data.school_school_url}`}>
                      <SelectOutlined key="setting" />
                    </a>,
                  ]}
                >
                  <Meta
                    title={<Title level={3}> {data.name} </Title>}
                    description={
                      <>
                        <b> City: </b> <br />{" "}
                        <Link to={`/locations/${data.city_id}`}>
                          {data.school_city}{" "}
                        </Link>{" "}
                        <br />
                        <b> Number of Students: </b> <br />{" "}
                        {data.student_size || "N/A"} <br />
                        <b> Tuition In-State (USD): </b> <br />{" "}
                        {data.cost_tuition_in_state || "N/A"} <br />
                        <b> Tuition Out-of-State (USD): </b> <br />{" "}
                        {data.cost_tuition_out_of_state || "N/A"} <br />
                        <b>
                          {" "}
                          Median Earnings 6 Years after Graduation (Annual):{" "}
                        </b>{" "}
                        <br />{" "}
                        {data.earnings_10_yrs_after_entry_median || "N/A"}{" "}
                        <br />
                        <b> Average Family Income of Student (Annual): </b>{" "}
                        <br />{" "}
                        {data.student_demographics_avg_family_income || "N/A"}{" "}
                        <br />
                        <b>
                          {" "}
                          Percentage of First Generation Students:{" "}
                        </b> <br />{" "}
                        {data.student_share_firstgeneration
                          ? `${parseFloat(
                              (
                                data.student_share_firstgeneration * 100
                              ).toPrecision(3)
                            )}%`
                          : "N/A"}
                      </>
                    }
                  />
                </Card>
              </div>
            </div>
            <div className="col-xs-12 col-sm-12 col-md-8 col-lg-8">
              <div className="p-3 text-center">
                <Card className="store-info-card">
                  <Title level={3}> Statistics </Title>
                  <div className="grid-container-med">
                    <div className="grid-item-med">
                      <Title level={5}> Ethnic Diversity </Title>
                      <DemoPieEthnicity {...data} />
                    </div>
                    <div className="grid-item-med">
                      <Title level={5}> Gender </Title>
                      <DemoPieGender {...data} />
                    </div>
                  </div>
                  <div className="grid-container-sm">
                    <div className="grid-item-med">
                      <Title level={5}> Retention Rate </Title>
                      <Progress
                        type="circle"
                        strokeColor={{
                          "0%": "#108ee9",
                          "100%": "#90EE90",
                        }}
                        percent={(
                          data.student_retention_rate_four_year_full_time_pooled *
                          100
                        ).toPrecision(2)}
                        format={(percent) => (percent ? `${percent}%` : "N/A")}
                      />
                    </div>
                    <div className="grid-item-med">
                      <Title level={5}> Admissions Rate </Title>
                      <Progress
                        type="circle"
                        strokeColor={{
                          "0%": "#108ee9",
                          "100%": "#90EE90",
                        }}
                        percent={(
                          data.admissions_admission_rate_overall * 100
                        ).toPrecision(2)}
                        format={(percent) => (percent ? `${percent}%` : "N/A")}
                      />
                    </div>
                    <div className="grid-item-med">
                      <Title level={5}> Average SAT </Title>
                      <Progress
                        type="circle"
                        strokeColor={{
                          "0%": "#FF0000",
                          "100%": "#90EE90",
                        }}
                        percent={(
                          (data.admissions_sat_scores_average_overall / 1600) *
                          100
                        ).toPrecision(2)}
                        format={(percent) =>
                          Math.ceil(percent / 100) * 1600
                            ? `${Math.ceil((percent / 100) * 1600)}`
                            : "N/A"
                        }
                      />
                    </div>
                    <div className="grid-item-med">
                      <Title level={5}> Average ACT </Title>
                      <Progress
                        type="circle"
                        strokeColor={{
                          "0%": "#FF0000",
                          "100%": "#90EE90",
                        }}
                        percent={(
                          (data.admissions_act_scores_midpoint_cumulative /
                            36) *
                          100
                        ).toPrecision(1)}
                        format={(percent) =>
                          Math.ceil(percent / 100) * 36
                            ? `${Math.ceil((percent / 100) * 36)}`
                            : "N/A"
                        }
                      />
                    </div>
                  </div>
                </Card>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

const columns = [
  {
    title: "Logo",
    dataIndex: "logo",
    key: "logo",
    render: (link) => {
      if (link === null || link === undefined) {
        // Placeholder image.
        return (
          <Avatar
            size={64}
            src="https://www.wodonnell.com/wp-content/uploads/2019/02/business-placeholder.png"
          />
        );
      }
      return <Avatar size={64} src={link} />;
    },
  },
  {
    title: "Name",
    dataIndex: "name",
    key: "name",
  },
  {
    title: "Alexa Rank",
    dataIndex: "alexa_rank",
    key: "alexa_rank",
    render: (data) => (data ? data : "N/A"),
  },
  {
    title: "Employees",
    dataIndex: "employees",
    key: "employees",
    render: (data) => (data ? data : "N/A"),
  },
  {
    title: "Industry",
    dataIndex: "industry",
    key: "industry",
    render: (data) => (data ? data : "N/A"),
  },
  {
    title: "Annual Revenue (estimated)",
    dataIndex: "estimated_annual_revenue",
    key: "estimated_annual_revenue",
    render: (data) => (data ? data : "N/A"),
  },
  {
    title: "Headquarters Location (latitude)",
    dataIndex: "location_latitude",
    key: "location_latitude",
  },
  {
    title: "Headquarters Location (longitude)",
    dataIndex: "location_longitude",
    key: "location_longitude",
  },
  {
    title: "Learn more",
    dataIndex: "company_id",
    key: "company_id",
    render: (id) => {
      return (
        <Link
          to={{
            pathname: `/companies/${id}`,
            state: { identifier: id },
          }}
        >
          <Button
            style={{ background: "#90EE90", borderColor: "#90EE90" }}
            type="primary"
            shape="round"
          >
            {" "}
            More{" "}
          </Button>
        </Link>
      );
    },
  },
];

/* Pagination code inspired by: https://gitlab.com/forbesye/fitsbits/-/blob/master/front-end/src/views/Politicians/GridView.js. */
export default function CollegeInstance() {
  const { state } = useLocation();

  const [loading, setLoading] = useState(true);
  const [data, setData] = useState({});

  const [loadingCompanies, setLoadingCompanies] = useState(true);
  const [companyIds, setCompanyIds] = useState([]);

  const history = useHistory();

  useEffect(() => {
    const fetcher = async () => {
      try {
        var id = undefined;
        if (state === undefined) {
          id = window.location.pathname.split("/")[2];
        } else {
          id = state.identifier;
        }

        const info = await view_college(id);
        setData(info);
        setLoading(false);

        let ids = [];
        const ids_obj = JSON.parse(info.companies_ids);
        for (let [_, value] of Object.entries(ids_obj)) {
          ids.push(value);
          if (ids.length === 15) {
            break;
          }
        }

        const all_objs = await Promise.all(ids.map((id) => view_company(id)));
        setCompanyIds(all_objs);
        setLoadingCompanies(false);
      } catch (e) {
        history.push("/void");
      }
    };
    fetcher();
  }, []);

  return (
    <>
      <div className="store-list-page-header">
        <PageHeader
          ghost={false}
          onBack={() => window.history.back()}
          title="School Profile"
          subTitle="See if this school is a right fit"
        />
      </div>

      {loading ? (
        <div style={{ textAlign: "center" }}>
          <Spin size="large" tip={`Loading Information for College ... `} />
        </div>
      ) : (
        <>
          <Divider>
            {" "}
            <Title level={3}> General Information </Title>{" "}
          </Divider>
          <CollegeInfo {...data} />
          <Divider>
            {" "}
            <Title level={3}> Map </Title>{" "}
          </Divider>
          <MapContainer
            {...{ latitude: data.location_lat, longitude: data.location_lon }}
          />
          <Divider>
            {" "}
            <Title level={3}> Nearby Companies </Title>{" "}
          </Divider>
          {loadingCompanies ? (
            <div style={{ textAlign: "center" }}>
              <Spin
                size="large"
                tip={`Loading Information for Nearby Companies... `}
              />
            </div>
          ) : (
            <>
              <Table
                columns={columns}
                dataSource={companyIds}
                scroll={{ x: true }}
                pagination={{
                  defaultPageSize: 5,
                }}
              />
            </>
          )}
        </>
      )}
    </>
  );
}
