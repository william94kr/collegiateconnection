import { React, useState, useEffect } from "react";
import "antd/dist/antd.css";
import "./styles/location.css";
import {
  PageHeader,
  Input,
  Spin,
  Avatar,
  Table,
  Button,
  Typography,
  Divider,
  Row,
  Col,
} from "antd";
import { Descriptions } from "antd";
import { view_all_companies } from "../library/client";
import { Link, useHistory } from "react-router-dom";
import { Menu, Select } from "antd";
// import { DownOutlined } from "@ant-design/icons";
import stateNames from "./static/States.json";
import Filter from "../components/Filter";
import Highlighter from "react-highlight-words";
import {
  NumberParam,
  StringParam,
  useQueryParams,
  withDefault,
} from "use-query-params";
import { unstable_renderSubtreeIntoContainer } from "react-dom";

const { Search } = Input;
const { Title } = Typography;
const { Option } = Select;

const highlight_generator = (searchString, data) => {
  console.log(searchString);
  if (searchString !== null && searchString !== undefined) {
    searchString = searchString.split(" ");
  }
  return (
    <Highlighter
      highlightClassName="YourHighlightClass"
      searchWords={searchString || []}
      textToHighlight={data}
    />
  );
};

/* Constants */

const industries = [
  "Pharmaceuticals",
  "Professional Services",
  "Industrial Conglomerates",
  "Diversified Telecommunication Services",
  "Trading Companies & Distributors",
  "Metals & Mining",
  "Household Durables",
  "Real Estate",
  "Banks",
  "Consumer Goods",
  "Marine",
  "Building Materials",
  "Media",
  "Commercial Services & Supplies",
  "Distributors",
  "Hotels, Restaurants & Leisure",
  "Internet Software & Services",
  "Machinery",
  "Renewable Electricity",
  "Gas Utilities",
  "Insurance",
  "Electrical Equipment",
  "Communications Equipment",
  "Aerospace & Defense",
  "Retailing",
  "Chemicals",
  "Education Services",
  "Beverages",
  "Textiles, Apparel & Luxury Goods",
  "Health Care Providers & Services",
  "Personal Products",
  "Construction & Engineering",
  "Technology Hardware, Storage & Peripherals",
  "Airlines",
  "Automotive",
  "Utilities",
  "Consumer Discretionary",
  "Biotechnology",
  "Diversified Financial Services",
  "Family Services",
  "Consumer Staples",
  "Transportation",
  "Air Freight & Logistics",
  "Diversified Consumer Services",
  "Food Products",
];

const revenueBounds = [
  "$0-$1M",
  "$1M-$10M",
  "$10M-$50M",
  "$50M-$100M",
  "$100M-$250M",
  "$250M-$500M",
  "$500M-$1B",
  "$1B-$10B",
  "$10B+",
];

const value_mapping_sort = {
  name: "Name (Asc.)",
  "-name": "Name (Desc.)",
  alexa_rank: "Alexa Rank (Asc.)",
  "-alexa_rank": "Alexa Rank (Desc.)",
  employees: "Employees (Asc.)",
  "-employees": "Employees (Desc.)",
};

/* Pagination code inspired by: https://gitlab.com/forbesye/fitsbits/-/blob/master/front-end/src/views/Politicians/GridView.js. */
export default function Business() {
  const [loading, setLoading] = useState(true);
  const [total, setTotal] = useState(0);
  const [page, setPage] = useState(1);
  const [perPage, setPerPage] = useState(20);
  const [companiesData, setCompaniesData] = useState([]);
  const [params, setParams] = useQueryParams({
    page: withDefault(NumberParam, 1),
    perPage: withDefault(NumberParam, 20),
    search: StringParam,
    sort: StringParam,
    annual_revenue: StringParam,
    employees: StringParam,
    alexa_rank: StringParam,
    industry: StringParam,
  });

  const history = useHistory();
  const [highlight, setHighlight] = useState([]);

  const cityState = (
    <Menu>
      {stateNames.map((data) => (
        <Menu.Item key="1"> {data.name} </Menu.Item>
      ))}
    </Menu>
  );

  useEffect(() => { // Use the useEffect hook to maintain previously-searched results
    const data = async () => {
      try {
        setLoading(true);
        const fetched = await view_all_companies(page, perPage, params);
        setLoading(false);
        setCompaniesData(fetched.companies || []);
        setTotal(fetched.total);
      } catch (e) {
        history.push("/void"); // Using caching to perserve search query parameters
      }
    };

    data();
  }, [page, perPage, params]);

  const columns = [
    {
      title: "Logo",
      dataIndex: "logo",
      key: "logo",
      render: (link) => {
        if (link === null || link === undefined) {
          // Placeholder image.
          return (
            <Avatar
              size={64}
              src="https://www.wodonnell.com/wp-content/uploads/2019/02/business-placeholder.png"
            />
          );
        }
        return <Avatar size={64} src={link} />;
      },
    },
    {
      title: "Name",
      dataIndex: "name",
      key: "name",
      render: (data) => highlight_generator(params.search, data),
    },
    {
      title: "Alexa Rank",
      dataIndex: "alexa_rank",
      key: "alexa_rank",
      render: (data) => (data ? data : "N/A"),
    },
    {
      title: "Employees",
      dataIndex: "employees",
      key: "employees",
      render: (data) => (data ? data : "N/A"),
    },
    {
      title: "Industry",
      dataIndex: "industry",
      key: "industry",
      render: (data) => (data ? data : "N/A"),
    },
    {
      title: "Annual Revenue (estimated)",
      dataIndex: "estimated_annual_revenue",
      key: "estimated_annual_revenue",
      render: (data) => (data ? data : "N/A"),
    },
    {
      title: "Headquarters Location (latitude)",
      dataIndex: "location_latitude",
      key: "location_latitude",
    },
    {
      title: "Headquarters Location (longitude)",
      dataIndex: "location_longitude",
      key: "location_longitude",
    },
    {
      title: "Learn more",
      dataIndex: "company_id",
      key: "company_id",
      render: (id) => {
        return (
          <Link
            to={{
              pathname: `/companies/${id}`,
              state: { identifier: id },
            }}
          >
            <Button
              style={{ background: "#90EE90", borderColor: "#90EE90" }}
              type="primary"
              shape="round"
            >
              {" "}
              More{" "}
            </Button>
          </Link>
        );
      },
    },
  ];
  return (
    <>
      <div className="page-header-wrapper">
        <PageHeader
          ghost={false}
          title={<b className="bolded"> Explore Companies </b>}
          subTitle="Browse/search for available comanies"
        >
          <Descriptions size="small" column={3}>
            <Descriptions>
              Learn more about the company of your dreams!
            </Descriptions>
          </Descriptions>
          <Search
            placeholder={params.search ? params.search : "Search"}
            allowClear
            enterButton="Search"
            size="large"
            onSearch={(value) => {
              setParams({ ...params, search: value, page: 1 });
              setHighlight(value.split(" "));
            }}
          />
          <Divider />
          <div style={{ paddingBottom: "10px" }}>
            <Title level={5}>
              {" "}
              <i> Search Customization </i>{" "}
            </Title>
          </div>
          <Row>
            <Col>
              <div>
                {" "}
                <b> Sorting </b>{" "}
              </div>
              <div style={{ paddingTop: "10px", paddingRight: "10px" }}>
                <Select
                  showSearch
                  style={{ width: 200 }}
                  placeholder={
                    params.sort ? value_mapping_sort[params.sort] : "Sort by"
                  }
                  onSelect={(val) => {
                    if (val === null) {
                      let another = {};
                      for (let prop in params) {
                        if (prop === "page") {
                          another[prop] = 1;
                        } else if (prop === "sort") {
                          another[prop] = undefined;
                        } else {
                          another[prop] = params[prop];
                        }
                      }
                      setParams(another);
                      return;
                    }
                    setParams({ ...params, sort: val, page: 1 });
                  }}
                > {/* options to sort the field by */}
                  <Option value="name">Name (Asc.) </Option>
                  <Option value="-name">Name (Desc.) </Option>
                  <Option value="alexa_rank">Alexa Rank (Asc.) </Option>
                  <Option value="-alexa_rank">Alexa Rank (Desc.) </Option>
                  <Option value="employees">Employees (Asc.) </Option>
                  <Option value="-employees">Employees (Desc.) </Option>
                </Select>
              </div>
            </Col>
            <Col>
              <div>
                {" "}
                <b> Industry </b>{" "}
              </div>
              <Filter
                options={industries
                  .map((industry) => [industry, industry])
                  .concat([["All", null]])}
                placeholder={params.industry ? params.industry : "Industry"}
                params={params}
                setParams={setParams}
                query={"industry"}
                setPage={setPage}
              />
            </Col>
            <Col>
              <div>
                {" "}
                <b> Revenue </b>{" "}
              </div>
              <Filter
                options={revenueBounds
                  .map((rev) => [rev, rev])
                  .concat([["All", null]])}
                placeholder={
                  params.annual_revenue ? params.annual_revenue : "Revenue"
                }
                params={params}
                setParams={setParams}
                query={"annual_revenue"}
                setPage={setPage}
              />
            </Col>
          </Row>
        </PageHeader>
      </div>
      {loading ? (
        <div style={{ textAlign: "center" }}>
          {" "}
          <Spin size="large" tip="loading companies..." />{" "}
        </div>
      ) : (
        <>
          <div style={{ padding: "24px" }}>
            <Table
              columns={columns}
              dataSource={companiesData}
              scroll={{ x: true }}
              pagination={{
                pageSize: params.perPage,
                defaultPageSize: 20,
                defaultCurrent: 1,
                current: params.page,
                total: total,
                showTotal: (total) => `${total} Companies`,
              }}
              onChange={(tableState) => {
                const { current, pageSize } = tableState;
                setParams({ ...params, page: current, perPage: pageSize });
              }}
            />
          </div>
        </>
      )}
    </>
  );
}
