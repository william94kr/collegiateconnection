import { React } from "react";
import "antd/dist/antd.css";
import "./styles/location.css";
import {
  PageHeader,
  Input,
  Select,
  Typography,
  Divider,
  Collapse,
  Tabs,
  Card,
} from "antd";
import { Descriptions } from "antd";
import ScatterPlot from "../components/ScatterPlot";
import IndustriesPieChart from "../components/IndustriesPieChart";
import HomerunBar from "../components/provider-visuals/HomerunBar";
import HandednessPieChart from "../components/provider-visuals/HandednessPieChart";
import { useHistory } from "react-router";
import { VideoSource } from "mapbox-gl";
import Chart from "../components/TimeZoneBarChart";
import HWScatter from "../components/provider-visuals/HeightWeightScatter";
const { Search } = Input;
const { Option } = Select;
const { Title } = Typography;
const Panel = Collapse.Panel;

const { TabPane } = Tabs;
const value_mapping_sort = {
  sat: "SAT (Asc.)",
  "-sat": "SAT (Desc.)",
  act: "ACT (Asc.) ",
  "-act": "ACT (Desc.)",
  accept_rate: "Acceptance Rate (Asc.)",
  "-accept_rate": "Acceptance Rate (Desc.)",
  in_state_tuition: "In-State Tuition (Asc.)",
  "-in_state_tuition": "In-State Tuition (Desc.)",
  out_state_tuition: "Out-of-State Tuition (Asc.)",
  "-out_state_tuition": "Out-of-State Tuition (Desc.)",
};

/* Pagination code inspired by: https://gitlab.com/forbesye/fitsbits/-/blob/master/front-end/src/views/Politicians/GridView.js. */
export default function Visualization() {
  return (
    <>
      <div className="page-header-wrapper">
        <PageHeader
          ghost={false}
          title={<b className="bolded"> Data Visualizations </b>}
        >
          <Descriptions size="small" column={3}>
            <Descriptions>
              Data insights from our webpage and our developers.
            </Descriptions>
          </Descriptions>
        </PageHeader>
      </div>

      <div style={{ padding: "25px" }}>
        <Tabs>
          <TabPane tab="Our Data" key="1">
            <Divider>
              {" "}
              <Title level={3}> Our Data </Title>{" "}
            </Divider>
            <div className="col-md-6 mx-auto col-lg-6 col-sm-12 col-xs-12">
              <Card
                title={
                  <h4 style={{ textAlign: "center" }}>
                    Average Tuition vs. Average SAT
                  </h4>
                }
                style={{
                  padding: "10px",
                  margin: "auto",
                }}
                className="store-info-card"
              >
                <ScatterPlot />
              </Card>
              <Card
                title={
                  <h4 style={{ textAlign: "center" }}>
                    Number of Businesses Per Industry
                  </h4>
                }
                style={{
                  padding: "10px",
                  margin: "auto",
                }}
                className="store-info-card"
              >
                <IndustriesPieChart />
              </Card>
              <Card
                title={
                  <h4 style={{ textAlign: "center" }}>
                    Number of Cities In Each TimeZone
                  </h4>
                }
                style={{
                  padding: "10px",
                  margin: "auto",
                }}
                className="store-info-card"
              >
                <Chart />
              </Card>
            </div>
          </TabPane>

          <TabPane tab="Developer Data" key="2">
            <Divider>
              {" "}
              <Title level={3}> Developer Data </Title>{" "}
            </Divider>
            <div className="col-md-6 mx-auto col-lg-6 col-sm-12 col-xs-12">
              <Card
                title={
                  <h4 style={{ textAlign: "center" }}>Homeruns by Teams</h4>
                }
                style={{
                  padding: "10px",
                  margin: "auto",
                }}
                className="store-info-card"
              >
                <HomerunBar />
              </Card>
              <Card
                title={
                  <h4 style={{ textAlign: "center" }}>Batting Handedness Per Player </h4>
                }
                style={{
                  padding: "10px",
                  margin: "auto",
                }}
                className="store-info-card"
              >
                <HandednessPieChart />
              </Card>

              <Card
                title={
                  <h4 style={{ textAlign: "center" }}>
                    Height vs Weight Correlation
                  </h4>
                }
                style={{
                  padding: "10px",
                  margin: "auto",
                }}
                className="store-info-card"
              >
                <HWScatter />
              </Card>
            </div>
          </TabPane>
        </Tabs>
      </div>
    </>
  );
}
