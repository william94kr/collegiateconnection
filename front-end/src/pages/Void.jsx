import errorpage from "./image/errorpage.png";
// code for the error page
export default function Void() {
  return (
    <>
      <img src={errorpage} width="1500" height="1000" alt="Error page" />
    </>
  );
}
