import React from "react";
import { Card, Col, Row, Spin } from "antd";
import TeamMemberCard from "../components/TeamMemberCard";
import ToolCard from "../components/ToolCard";
import { Typography, Divider } from "antd";
import "./styles/about.css";

import kevinImage from "./static/kevinli.png";
import justinImage from "./static/justinfoster.png";
import adityaImage from "./static/adityagupta.png";
import mathewImage from "./static/mathewtucci.png";
import sungwookImage from "./static/sungwookkim.png";

const { Meta } = Card;
const { Title } = Typography;

// Gitlab info.

const gitLabInfo = async () => {
  const teamInfo = [
    {
      name: "Kevin Li",
      alias: null,
      username: "li.kevin",
      email: "likevinsen@gmail.com",
      role: "Full-Stack",
      image: kevinImage,
      commits: 0,
      issues: 0,
      tests: 10,
      bio: `
                Kevin Li is a Junior at the University of Texas at Austin studying Computer Science. In the past,
                he's worked at ClassPass as a Software Engineering Intern on the Machine Learning + Data team. In
                his free time, he enjoys playing ultimate frisbee, running, and walking his dog.
            `,
    },
    {
      name: "Aditya Gupta",
      alias: "AdityaG2000",
      username: "aditya",
      email: "Aditya2000@utexas.edu",
      role: "Full-Stack",
      image: adityaImage,
      commits: 0,
      issues: 0,
      tests: 8,
      bio: "Aditya Gupta is a Senior at the University of Texas studying Computer Science. He worked in the past at Amazon and Intel as a Software Engineering Intern. In his free time, he enjoys playing video games, running and learning new algorithms",
    },
    {
      name: "Justin Foster",
      alias: null,
      username: "justinfoster",
      email: "jrfoster2416@gmail.com",
      role: "Full-Stack",
      image: justinImage,
      commits: 0,
      issues: 0,
      tests: 0,
      bio: `
                    Justin Foster is a Junior at the University of Texas - Austin studying Computer Science. 
                    He loves to build and create, whether that is legos or complex software applications. 
                    Passionate about all things space and wanting to work in Space Exploration and research, 
                    Justin is currently involved with Texas Spacecraft Laboratory developing machine learning tools
                    to research pose estimation for oribiting satellites.`,
    },
    {
      name: "Mathew Tucciarone",
      username: "mat4987",
      email: "mat4987@utexas.edu",
      role: "Full-Stack",
      image: mathewImage,
      commits: 0,
      issues: 0,
      tests: 10,
      bio: "Mathew Tucciarone is a Senior Computer Science Major. He worked as a Jr. Software Developer for Resermine Inc doing Web development. In his free time he enjoys watching sports such as the Texas Longhorns beating Oklahoma. He is on the University of Texas Ice Hockey Team.",
    },
    {
      name: "William (Sungwook) Kim",
      alias: "Sungwook",
      username: "william94kr",
      email: "brian94kr@gmail.com",
      role: "Full-Stack",
      image: sungwookImage,
      commits: 0,
      issues: 0,
      tests: 34,
      bio: ` Sungwook Kim is a Junior at the University of Texas at Austin studying Computer Science. 
                    He is interested in the automation system, IoT(Internet of Things), and cloud computing. 
                    His goal is to get a job regarding those fields and contribute to them. In his free time,
                    he enjoys playing golf, bowling, and singing in Karaoke.
                `,
    },
  ];

  const urls = [
    "https://gitlab.com/api/v4/projects/30365609/issues?per_page=1000",
    "https://gitlab.com/api/v4/projects/30365609/repository/contributors",
  ];

  const rawData = await Promise.all( // send a request to GitLab's RESTful API in order to get team contribution statistics
    urls.map(async (url) => (await fetch(url)).json())
  );

  const issues = rawData[0];
  const contributors = rawData[1];

  for (let issue of issues) {
    const { name, username } = issue.author;

    for (let member of teamInfo) {
      if (
        name === member.name ||
        name === member.alias ||
        username === member.username
      ) {
        member.issues += 1;
        break;
      }
    }
  }

  let totalCommits = 0;
  for (let contributor of contributors) {
    for (let member of teamInfo) {
      const { name, email, commits } = contributor;
      if (
        name === member.name ||
        name === member.alias ||
        email === member.email
      ) {
        member.commits += commits;
        totalCommits += commits;
      }
    }
  }

  const totalIssues = issues.length;
  return {
    team: teamInfo,
    commits: totalCommits,
    issues: totalIssues,
    tests: 54, // tests are hardcoded
  };
};

const tools = [ // tools used for our project and how we used them
  {
    name: "MapBox",
    image:
      "https://assets.website-files.com/5d3ef00c73102c436bc83996/5d3ef00c73102c1f23c83a2a_logo-reversed.png",
    description: "Used to show location of cities, colleges, and companies",
    link: "https://github.com/alex3165/react-mapbox-gl",
  },
  {
    name: "Clear Bit API",
    image: "https://clearbit.com/dist/site/logo.png",
    description: "Used to scrape information about companies.",
    link: "https://clearbit.com/",
  },
  {
    name: "College Scorecard API",
    image:
      "https://www.title9.us/wp-content/uploads/US-DeptOfEducation-Seal.svg",
    description: "Used to scrape information about colleges.",
    link: "https://collegescorecard.ed.gov/",
  },
  {
    name: "Teleport API",
    image: "https://teleport.org/assets/firefly/logo.png",
    description: "Used to obtain information/statistics about cities.",
    link: "https://developers.teleport.org/api/",
  },
  {
    name: "Docker",
    image: "https://miro.medium.com/max/336/1*glD7bNJG3SlO0_xNmSGPcQ.png",
    description: "Used to containerize applications for deployment.",
    link: "https://www.docker.com/",
  },
  {
    name: "AWS",
    image:
      "https://yt3.ggpht.com/ytc/AKedOLQP0vNXjkoKrCAYvWyOm9vEhDuBNytjbpEYi1ugD7w=s900-c-k-c0x00ffffff-no-rj",
    description: "Used to host/deploy website over AWS Amplify",
    link: "https://aws.amazon.com/",
  },
  {
    name: "React",
    image:
      "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a7/React-icon.svg/1200px-React-icon.svg.png",
    description: "Used to create frontend components and pages",
    link: "https://reactjs.org/",
  },
  {
    name: "Ant Design",
    image:
      "https://gw.alipayobjects.com/zos/rmsportal/KDpgvguMpGfqaHPjicRK.svg",
    description:
      "Used to design frontend componets and pages as an UI design library",
    link: "https://ant.design/components/overview/",
  },
  {
    name: "Postman",
    image:
      "https://mms.businesswire.com/media/20210818005151/en/761650/22/postman-logo-vert-2018.jpg",
    description:
      "Used to request information from external APIs. Also used for unit testing endpoints",
    link: "https://www.postman.com/",
  },
  {
    name: "GitLab",
    image:
      "https://images.g2crowd.com/uploads/product/image/social_landscape/social_landscape_15680ee909406e13c21c8f179f83d99e/gitlab.png",
    description:
      "Used for hosting the code repository, issue tracking, and continuous integration (via GitLab pipeline).",
    link: "https://about.gitlab.com/",
  },
  {
    name: "SQLAlchemy",
    image: "https://www.sqlalchemy.org/img/sqla_logo.png",
    description:
      "Used as an Object Relational Mapper between python class and relational database",
    link: "https://www.sqlalchemy.org/",
  },
  {
    name: "PostgreSQL",
    image: "https://www.postgresql.org/media/img/about/press/elephant.png",
    description: "Used as Open Source Relatonal Database Management System",
    link: "https://www.postgresql.org/",
  },
  {
    name: "Selenium",
    image:
      "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAANwAAADlCAMAAAAP8WnWAAAAkFBMVEUAtAD///8AsgCO1I9gwGEArgAAsAAArQCk3KP39/jq9+r3/Pes36zF6MX9//2f2p/Z8Nne8t5lx2WC0IJAvkC24rbx+vFryWuY2Jjj9OPM68wouSg4vDiI0oi55LlQwlBbxVtzzHN5zXkvui9LwEsbtxvS7dJdxV2U1pPI6ch2yHf++v7A5r93zXZEv0S447eha/c4AAAMEUlEQVR4nO2daXuCvBKGaTyJvrjgUnetW7XtObbv//93x60KZDIziRQMl8/XSshdQpYnkyH457//KYcq//snrWCkgnKo2nxJK6iIomuVkapheeFUQ2MrDZxY6mylgQsAtrLAVdvlhVMjiK0ccOIdZCsJXL28cNUJzFYGODU3sJUBbmZiKwGcGpQXTu6NbN7DibWZzXu4KcLmO5zS1zmlgZN9jM1vODFE2fyG2+JsXsPJcXnhVI1g8xgONBbKAhe0ygsnQWOhHHAGY6EUcCZjoRxwBmOhDHBGY6EEcGZjoQRwZmPBfzjEWIipHvoIhxkLN7Wr1aZ/cKixcFWoAjH3Dw41Fq5t8vBDUfEOTqLGwq+mwkc4wli46OcI5R0cZSyc1T0xeQdHGQsn9c4BGr7BkcbCUZE8/9gzONpYOOitevm1X3AMY+HlZVe9/twrOIax8DK+BUR5BQdHLCQ1iNH4BMcxFlrb2AUewbGMhVkcxic4hrGwTLD4AycZxsIqGV3pDRzHWKikIke9gWMYCw2ZusYXOIax0KymL/IEjmEstNPPzRc4hrEQApHafsDRxkIduqw4OMG/LcNYmEKlFQan1nPufRnGwjtYVlFwxyF5wLsxw1jowiUVBHeeA49ZxzVoY6FnKKcYOLE412qnd9+aaGMhMpVS0JP7rddeG3jToo2FN2MZhcDJzrVm2pQpJdpYQJ5+EXCJcynpyW5alLGAvbcFwKV6vxVGRxoLaI9bAFx6urE03580FhKugqb84fQAyZmpArSxYLz0fH3ecND83vTvJ40F5KGfrs8ZTvWAOg7g35LGAvq6BrnDCXhBDXZ5pLFAdbS5wxkaGrDSJI0FaojMG87c0HSPgDIW9Cs05QqnPvnPgTIWoGedVq5wC6y2n4k3iDIWIFdBU55wCp/ff8frSxgLoKugKVc44i2KjVqUsQC6CpryhBMrvMYvr781UYSx8MNcw+faoUAjeOKBXCpFGAsGV0FTvkMBNS5fXiWB/8rkKmjKeRBXFbzep06QMBaMroKmvOeWEhnqjmpXRRU3Fsyugqb8VwUR8ewifLrM8ZR+lf96TnJCSYziuYEXFbASl0BqD66YPu5FRRhE7nS4q6CpEGuvyooqBIS7CpqKMWWrb05sC8uaFuQ4V1kh2ClRroKmonZ5gExPlEhXQVNh+3OcsJKEaFdBU3E7q3JnxcZwFTQVuCduRcdxFTQVueGvGBGGF7FcBU2FRjOoDo11Es9V0FRsqAbhqlzFcxU0FRyHwqNjugqaig6yEYyzK1xXQS+86Agimo7tKuhlFw0XBIThF7mn33wAOJzuzWWAu+gR4AJkj3HnMDG56iHgtkY6K1dB00PAia0hIMPOVdDLfQS4QExBOktXQS/2IeACsYHgLF0FvdTHgAM3y4lYBUahznAirjtrcSzuNc1m7SroZTrACaFk8NNdV0ZRrdGoRaPe6l1Ipe58+3+SbPaugl6kLZyQ2+9GW++7W53+/EfeA5iMF3JwFfQSreCEXDSwuWDrqxe484nYnrmLq6AXaAGntjXGQaj2PHBtULfgSidXQS+PDadmbDeuuZCOndTHuQA3V0EvjgmntlZG43jlhqe6x6sdXQVNTDhqV01XOHRqWWp4eHMdXQVNLDgxZXodCXV+XBqX2A4drjKUxYBTrHQIgN6cRvfsJkwMOMnJHgar5dY2sxINd98271uR3+gg4ZR1V5LUYFPcvJyC4+X7QfWeG4xWeQKOm2AL0b6wlknA8RJs4aoV1i5xOOiDKbbKaCrlIhyOlYSK0DRHmpRQOOWyLZ/SqkATA4UznzkcN9aL7WEusd0sVp//Itts/SJHcQxOmIbv2lbeJlZCKKUWEQwYPuwMRcJL0ya0GBUqqAB8dxqPdwqDgyPlRyYDQMhZOjDIeWctGyFwcKtEYzmVSER1NQr++BsCBwYbtIj6qu3X9bdj6xfu9P6elIUVisJB/n1E3lQuf19VO8vwAPTei/pfu91u0uxHvYW4yyg8lWmGAweCH041z6/eh0XVlOr2tdV+uF/fYRQGKBzYn7DuJY9Ld/52r5BL42xh930HnhkOPrnAq7GYjobs4H/VQ/f8W5G7D2qG+4ZuxS6Xe2hDzmmjN3J8erZw39mOXGrByqFa/8g49gs+VpTtfEqxw4GdvBgEDs6KkMkOxeUOM1Zy97MGDk4t0lsaHIZ2VsaiWvHRjrLfaMWmX6a7fKosplX2dmg3wwBuafZPGjPHbZxY6Q52qO2zw6ZfX8h9xtHrXbMj+52Vo17t7og1S+KsW32/3roCcr83kFLL7i7Yeo5hD4VvvakDICvxLaSO1UiErsR5Icj1ZsW2ibIyTYOyClBEPRTq/OxNrUllxgc0/tfCxmoq5EHbD4Mp8zLLCC5QFoPsYZjtD3mxKKYsBvufm+8khNyAZ7VsGiYKJ6xf+69vxhgIN8rOLH2lmkJegIURijvOvE8DJPVGhTJIcIgBfacqMNAP+I+O2Ahx2uQJ52jrBBMLGZZ/Clia9NiPjtrl4fcpcbVGZjzwCI9xaQtUgL8wofbnXM+XtnqmKkCNYWR+UYH8j2xzhtw25n21AtBY6yBOgpZwbWwZpU9xm9mlL3Df8Af7CGitge5yia72+8yeHPxS8zTR/8XQHjuxytZTNXC9Dk6Qjdi4BBAdFer/KeA054a4vdancHfZebFfVLoIo7TYeaX/hnbd01fUmf0lM7BNBI79SvrUA7AcIB16/Qwhc4LJjrdUWze8ZuK/LIApB7m+1pP2MMdxi0hZJT5dAjcSFYFW94eCcUlt1dxnGt82Mc5CLvqMT5CkKx+HAyar/RqhSJs/d/4A7lg7texbPr94Tw/0Jy5i9ij25wqEkpuK1fH82Ma4+xI8qczfuSTgYjThttDG7QbasQ9H8brLO467KDmbNzlNNLbV7GoMpcWbO993UOkAOO3tSTPiWhVn1yst3gTs/lNYB8DXEZ6IoJE5HC9jfjZHzIQK5ogPeO25M4Pj1Tmz83NCIidGrnAfGcGN8oU7SG1MT++6YMvqyfECVLM9+WjKJLf8/QHrO9kPCmdatl+9xiyiU4+iY33+AC6oguva6+SZSO7I1ZhXmazhAMfjJda5QVGOc2pVoCuzlbhawDaW6ecoHBQs93fR67S1d+jg/7XYfAAd+FuzBGxQ5vrFQRTceQt0x78/tGJ7GV7hICetsCd3OXnPjwIBm+XtxAvUo/BGZAdRewXX57Bmrg/BOcjt71C+WK6ZZS3+iZAJK7gG3OaI1V5AUQzsD0dZCt98TD6GCh18Ao8E8VcWzDCRDYteG/TJpeowoM4Qw8kxEvMJcD98kl1AWaI6CFxVb2LhN4anDFPH+Ak6+EDeJ9UdC5eQLCyCCAyyqUdTA58wfdkp2WHAUQRrYjNkGO7yiNo7jLqjY9xJ8qrDjOjTZDc0Ej81RBHMkS5TBcelol2ADQ6HngtsTUbdqZKXQwBSLEZIPsfULo4hAOTNtNMsfo/NWicRMcJxdlQH4/bua7Ib4w5Renpjyuw/WALt/dAioquFaHsUzwTH+FwfV9pRBGPWzl0S7zD7F6u4/Wt7xMQEp+ySoiL60huT2Xce1Jbi3Nilmg61TQLL1HRGuAwOPZ4FlI5/ciJsT3btDvzGv2YCFwSuW8UpraHSnROMD6weHdJb2mdaBrSHXxPJTyeblNWpNSzG2XUjPCbjQpSTtBPUIhu4w2zKKiQRUGieVbjSWaybiVWBuK9bGWMzJuH2UtskQyAWq/LjjocHxNjEhUa/m7Tjo9E2g3E2TOuTWsdA0ZSE1lZrI4a1t3VqmyEjA5Fa2LWLveVJG1YOoo09XoU1UxI2mS3a73/zvQK1bdiEaLQi9mFhNWOOeLvF36X0V2rNnW2O8SjgtOSCUXB/9rdZ74+7p3Q1xjX7M0xq00Dfvcna8diXXQSRUh/RzliRTn8duJ3OEnJpyL7YaXTdE2fa7vIcE3duu5XGVycctF5arXp9EI53zcbncKbuSt8pZDAc7dthvfVb6L7We73vqL/TFpY4pRk4rruklJdsAxn4qr+lykupd5f5KAlz/0RPOF/1hPNVTzhf9YTzVU84X/WE81VPOF/1hPNVTzhf9YTzVU84X3WAC4lD6P5qNvg/qJqbnYseZx8AAAAASUVORK5CYII=",
    description:
      "Used for acceptance tests of the GUI. Also used to scrape data from Wikipedia",
    link: "https://www.selenium.dev/",
  },
  {
    name: "Discord",
    image:
      "https://www.net-aware.org.uk/siteassets/images-and-icons/application-icons/app-icons-discord.png?w=585&scale=down",
    description: "Used as team communication medium",
    link: "https://discord.com/",
  },
];

class About extends React.Component {
  constructor() {
    super();
    this.state = { data: null };
  }
  async componentDidMount() {
    const resp = await gitLabInfo(); // synchronously wait for the gitlab information that is needed for the about page
    this.setState({
      data: resp.team,
      commits: resp.commits,
      issues: resp.issues,
      tests: resp.tests,
    });
  }
  render() {
    return (
      <>
        <Divider>
          <Typography>
            <Title level={2}>
              What is <b className="bolded">Collegiate</b>Connection?{" "}
            </Title>
          </Typography>
        </Divider>
        <div style={{ padding: "15px", paddingTop: "0px" }}>
          <Row gutter={16}>
            <Col span={12}>
              <Card bordered={false} className="store-info-card">
                <Meta
                  title={
                    <Typography>
                      <Title level={4}> Our Mission Statement </Title>
                    </Typography>
                  }
                  description={
                    <div>
                      <b className="bolded">Collegiate</b>Connection is an
                      business/college data aggregator. Our website makes it
                      easier for students to plan their future after college. We
                      know life after college is scary so Collegiate Connection
                      is doing its part to make it a little easier.
                      <br></br>
                      <br></br>
                      In response to the prevalence of information not specific
                      to locations around colleges, we want to focus on how
                      college factors into where you can work after your degree.
                      For each college in the U.S., college information taken
                      can compare and stack up data around college, cities, and
                      businesses surounding.
                    </div>
                  }
                ></Meta>
              </Card>
            </Col>
            <Col span={12}>
              <Card bordered={false} className="store-info-card">
                <Meta
                  title={
                    <Typography>
                      <Title level={4}> Data Insights </Title>
                    </Typography>
                  }
                  description={
                    <div>
                      We scrape from a variety of APIs and websites (including
                      Teleport, Google Places, Clearbit) to aggregate city
                      locations and business facts. We are able to glean
                      important insights about:
                      <br></br>
                      <ol>
                        <li>
                          {" "}
                          This will answer which university is best in regards
                          to all aggregate data surrounding its local businesses
                          and city statistics (i.e. cost of living, travel
                          connectivity, environmental quality, career
                          opportunities).{" "}
                        </li>
                        <li>
                          {" "}
                          Which cities have the potential to provide career
                          options for certain industries (e.g. Rust Belt states’
                          cities specialize in factories).{" "}
                        </li>
                        <li>
                          This will answer which university has the best
                          opportunities for careers right out of college (e.g.
                          universities in technology hubs may have access to
                          more careers).
                        </li>
                      </ol>
                      <div>Interesting Observations:</div>
                      <ol>
                        <li>
                          Smaller cities tend to have much less information
                          about them which can possibly be attributed to the
                          lack of ability to digitize and popularize this
                          information.
                        </li>
                        <li>
                          We were Expecting there to be some colleges and cities
                          with no companies, but it was very surprising to
                          discover that there are so many smaller colleges in
                          smaller cities that still have many businesses started
                          locally.
                        </li>
                      </ol>
                    </div>
                  }
                ></Meta>
              </Card>
            </Col>
          </Row>
          <Divider>
            <Typography>
              <Title level={2}> The Team </Title>
            </Typography>
          </Divider>
          {this.state.data ? (
            <>
              <div className="grid-container-members">
                <div className="grid-item">
                  <TeamMemberCard {...this.state.data[0]} />
                </div>
                <div className="grid-item">
                  <TeamMemberCard {...this.state.data[1]} />
                </div>
                <div className="grid-item">
                  <TeamMemberCard {...this.state.data[2]} />
                </div>
                <div className="grid-item">
                  <TeamMemberCard {...this.state.data[3]} />
                </div>
                <div className="grid-item">
                  <TeamMemberCard {...this.state.data[4]} />
                </div>
              </div>
            </>
          ) : (
            <div style={{ textAlign: "center" }}>
              {" "}
              <Spin size="large" />{" "}
            </div>
          )}
        </div>
        <Divider>
          {this.state.data ? (
            <Card bordered={false} className="store-info-card">
              <Meta
                title={
                  <Typography>
                    <Title level={4}> Additional Information </Title>
                  </Typography>
                }
                description={
                  <div>
                    <b> Total Commits: </b> {this.state.commits} <br />
                    <b> Total Issues: </b> {this.state.issues} <br />
                    <b> Total Tests: </b> {this.state.tests} <br />
                    <a href="https://gitlab.com/justindeanfoster/collegiateconnection">
                      GitLab Repo
                    </a>
                    ,
                    <a href="https://documenter.getpostman.com/view/17763762/UV5Xixvn">
                      {" "}
                      Postman
                    </a>
                    .
                  </div>
                }
              ></Meta>
            </Card>
          ) : (
            <div style={{ textAlign: "center" }}>
              {" "}
              <Spin size="large" />{" "}
            </div>
          )}
          <br />
          <Typography>
            <Title level={2}> APIs/Tools Used </Title>
          </Typography>
        </Divider>

        <div className="grid-container">
          {tools.map((tool) => {
            return (
              <div className="grid-item">
                <ToolCard {...tool} />
              </div>
            );
          })}
        </div>
      </>
    );
  }
}

export default About;
