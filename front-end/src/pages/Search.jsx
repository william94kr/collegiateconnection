import { React, useState, useEffect } from "react";
import "antd/dist/antd.css";
import "./styles/location.css";
import {
  PageHeader,
  Input,
  Spin,
  Typography,
  Row,
  Col,
  Divider,
  Empty,
} from "antd";
import { Descriptions } from "antd";
import CollegeCard from "../components/CollegeCard";
import CompanyCard from "../components/CompanyCard";
import LocationCard from "../components/LocationCard";
import MoreCard from "../components/MoreCard";

import {
  view_all_cities,
  view_all_colleges,
  view_all_companies,
} from "../library/client";
import { useHistory } from "react-router";
import Highlighter from "react-highlight-words";
import {
  StringParam,
  useQueryParams,
} from "use-query-params";
const { Search } = Input;
const { Title } = Typography;

/* Pagination code inspired by: https://gitlab.com/forbesye/fitsbits/-/blob/master/front-end/src/views/Politicians/GridView.js. */
export default function SearchTab() {
  const [loading, setLoading] = useState(true);
  const [total, setTotal] = useState(0);
  const [page, setPage] = useState(1);
  const [perPage, setPerPage] = useState(4);
  const [collegesData, setCollegesData] = useState([]);
  const [citiesData, setCitiesData] = useState([]);
  const [companiesData, setCompaniesData] = useState([]);
  const [highlight, setHighlight] = useState([]);

  const [params, setParams] = useQueryParams({
    search: StringParam,
  });

  const history = useHistory();

  useEffect(() => {
    const data = async () => {
      try {
        setLoading(true);
        const fetchedColleges = await view_all_colleges(page, perPage, params);
        const fetchedCities = await view_all_cities(page, perPage, params);
        const fetchedCompanies = await view_all_companies(
          page,
          perPage,
          params
        );
        setLoading(false);
        setCollegesData(fetchedColleges.colleges || []);
        setCitiesData(fetchedCities.cities || []);
        setCompaniesData(fetchedCompanies.companies || []);
        setPage(page);
        setTotal({
          colleges: fetchedColleges.total,
          cities: fetchedCities.total,
          companies: fetchedCompanies.total,
        });
      } catch (e) {
        history.push("/void");
      }
    };

    data();
  }, [page, perPage, params, highlight]);
  return (
    <>
      <div className="page-header-wrapper">
        <PageHeader
          ghost={false}
          title="General Search"
          subTitle={
            params.search
              ? `Showing results for: "${params.search}"`
              : "Browse/search for colleges, cities, and businesses"
          }
        >
          <Descriptions size="small" column={3}>
            <Descriptions label="Last Updated">10/24/2021</Descriptions>
          </Descriptions>
          <Search
            placeholder="The University of Texas at Austin"
            allowClear
            enterButton="Search"
            size="large"
            onSearch={(value) => {
              setPage(1);
              setParams({ ...params, search: value });
              setHighlight(value.split(" "));
            }}
          />
        </PageHeader>
      </div>
      {loading ? (
        <div style={{ textAlign: "center" }}>
          {" "}
          <Spin size="large" tip="loading results..." />{" "}
        </div>
      ) : (
        <>
          <Row>
            <Col span={22} offset={1}>
              <Divider orientation="left">
                {" "}
                <h4>
                  {" "}
                  <b className="bolded">Colleges </b>{" "}
                </h4>{" "}
              </Divider>
              {collegesData.length > 0 && total.colleges ? (
                <div className="grid-container">
                  {collegesData.map((college) => (
                    <div className="grid-item">
                      <CollegeCard
                        data={{
                          ...college,
                          searchString: params.search,
                          className: "itemcard-no-shadow",
                        }}
                      />
                    </div>
                  ))}
                  <div className="grid-item">
                    <MoreCard
                      data={{
                        nums: total.colleges,
                        category: "colleges",
                        searchStr: params.search,
                      }}
                    />
                  </div>
                </div>
              ) : (
                <div style={{ textAlign: "center" }}>
                  {" "}
                  <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} />{" "}
                </div>
              )}
            </Col>

            <Col span={22} offset={1}>
              <div style={{ paddingTop: "10px" }}>
                <Divider orientation="left">
                  {" "}
                  <h4>
                    {" "}
                    <b className="bolded"> Companies </b>{" "}
                  </h4>{" "}
                </Divider>
              </div>
              {companiesData.length > 0 ? (
                <div className="grid-container">
                  {companiesData.map((company) => (
                    <div className="grid-item">
                      {" "}
                      <CompanyCard
                        data={{
                          ...company,
                          searchString: highlight,
                          className: "itemcard-no-shadow",
                        }}
                      />{" "}
                    </div>
                  ))}
                  <div className="grid-item">
                    <MoreCard
                      data={{
                        nums: total.companies,
                        category: "businesses",
                        searchStr: params.search,
                      }}
                    />
                  </div>
                </div>
              ) : (
                <div style={{ textAlign: "center" }}>
                  {" "}
                  <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} />{" "}
                </div>
              )}
            </Col>
            <Col span={22} offset={1}>
              <div style={{ paddingTop: "10px" }}>
                <Divider orientation="left">
                  {" "}
                  <h4>
                    {" "}
                    <b className="bolded"> Cities </b>{" "}
                  </h4>{" "}
                </Divider>
              </div>
              {citiesData.length > 0 ? (
                <div className="grid-container">
                  {citiesData.map((city) => (
                    <div className="grid-item">
                      {" "}
                      <LocationCard
                        data={{
                          ...city,
                          searchString: highlight,
                          className: "itemcard-no-shadow",
                        }}
                      />{" "}
                    </div>
                  ))}
                  <div className="grid-item">
                    <MoreCard
                      data={{
                        nums: total.cities,
                        category: "locations",
                        searchStr: params.search,
                      }}
                    />
                  </div>
                </div>
              ) : (
                <div style={{ textAlign: "center" }}>
                  {" "}
                  <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} />{" "}
                </div>
              )}
            </Col>
          </Row>
        </>
      )}
    </>
  );
}
