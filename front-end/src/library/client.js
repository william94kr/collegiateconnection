const axios = require('axios')

const BASE_URL = 'https://api.collegiateconnection.me'


const view_all_cities = async (page=1, per_page=20, params={}) => {
    if (params.page !== undefined && params.page !== null) {
        page = params.page
    }

    if (params.perPage !== undefined && params.perPage !== null) {
        per_page = params.perPage
    }

    let query = `${BASE_URL}/cities?page=${page}&per_page=${per_page}`
    for (let key in params){
        if (params[key] === undefined || params[key] === null){
            continue
        }
        query += `&${key}=${encodeURIComponent(params[key])}`
    }
    const data = (
        await axios.get(query)
    ).data

    return data
}

const view_all_colleges = async (page=1, per_page=20, params={}) => {
    if (params.page !== undefined && params.page !== null) {
        page = params.page
    }

    if (params.perPage !== undefined && params.perPage !== null) {
        per_page = params.perPage
    }
    let query = `${BASE_URL}/colleges?page=${page}&per_page=${per_page}`

    for (let key in params){
        if (params[key] === undefined || params[key] === null){
            continue
        }
        query += `&${key}=${encodeURIComponent(params[key])}`
    }
    const data = (
        await axios.get(query)
    ).data

    console.log(data)

    return data
}

const view_all_companies = async (page=1, per_page=20, params={}) => {
    if (params.page !== undefined && params.page !== null) {
        page = params.page
    }

    if (params.perPage !== undefined && params.perPage !== null) {
        per_page = params.perPage
    }

    let query = `${BASE_URL}/companies?page=${page}&per_page=${per_page}`

    for (let key in params) {
        console.log(`here: ${key}, ${params[key]}`)
        if (params[key] === undefined || params[key] === null) {
            continue
        }
        query += `&${key}=${encodeURIComponent(params[key])}`
    }
    console.log(query)
    return (
        await axios.get(query)
    ).data
}

const view_company = async (id) => {
    return (
        await axios.get(`${BASE_URL}/companies/${id}`)
    ).data
}

const view_college = async (id) => {
    try{
        return (await axios.get(`${BASE_URL}/colleges/${id}`)).data
    } catch(e) {
        console.error(`missing ${id}`)
        return null
    }
}

const view_city = async (id) => {
    return (
        await axios.get(`${BASE_URL}/cities/${id}`)
    ).data
}

export { 
    view_all_cities,
    view_all_colleges,
    view_all_companies,
    view_college,
    view_company,
    view_city
}
